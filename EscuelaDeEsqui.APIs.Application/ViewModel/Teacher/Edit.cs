﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.APIs.Application.ViewModel.Teacher
{
    public class Edit
    {
        public int TeacherId { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(50)]
        public string Surname1 { get; set; }
        [StringLength(50)]
        public string Surname2 { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        [Phone]
        public string Phone { get; set; }
        [StringLength(9)]
        public string DNI { get; set; }
        [StringLength(50)]
        public string Nationality { get; set; }
        [StringLength(11, MinimumLength = 11)]
        public string SocialSecurityNumber { get; set; }
        [Required]
        public decimal IRPFPerfectage { get; set; }
        [Required]
        public Domain.Entities.Teacher.TittleEnum Tittle { get; set; }
        public Domain.Entities.Teacher.StateEnum State { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        public decimal Salary { get; set; }
        [StringLength(4, MinimumLength = 4)]
        public string IBAN { get; set; }
        [StringLength(4, MinimumLength = 4)]
        public string BankIdentifier { get; set; }
        [StringLength(4, MinimumLength = 4)]
        public string BranchIdentifier { get; set; }
        [StringLength(10, MinimumLength = 10)]
        public string AccountNumber { get; set; }
    }
}
