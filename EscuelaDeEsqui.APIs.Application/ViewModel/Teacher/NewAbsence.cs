﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.APIs.Application.ViewModel.Teacher
{
    public class NewAbsence
    {
        public int TeacherId { get; set; }
        [DataType(DataType.Date)]
        public DateTime date { get; set; } = DateTime.Today;
        [Required]
        public Domain.Entities.Absence.DurationEnum Duration { get; set; }
        [Required]
        public Domain.Entities.Absence.AbsenceTypeEnum AbsenceType { get; set; }
        [StringLength(100)]
        public string Comments { get; set; }
    }
}
