﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.APIs.Application.ViewModel.Client
{
    public class PersonalInformation
    {
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(50)]
        public string Surname1 { get; set; }
        [StringLength(50)]
        public string Surname2 { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Phone]
        public string Phone { get; set; }
        [StringLength(50)]
        public string Nationality { get; set; }
        [StringLength(9)]
        public string DNI { get; set; }
        [StringLength(75)]
        public string CompanyName { get; set; }
        [StringLength(9)]
        public string CompanyCIF { get; set; }
    }
}
