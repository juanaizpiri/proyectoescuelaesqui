﻿using EscuelaDeEsqui.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.APIs.Application.ViewModel.Lesson
{
    public class Create
    {
        public IEnumerable<Student> Students { get; set; } = new List<Student>();
        public TimeSpan Duration { get; set; }
        public DateTime DateAndTime { get; set; }
        public string Comments { get; set; }
        public int LessonTypeId { get; set; }
        public Student student { get; set; }
    }
}
