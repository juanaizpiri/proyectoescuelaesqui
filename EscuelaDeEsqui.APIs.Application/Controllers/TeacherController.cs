﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using EscuelaDeEsqui.APIs.Application.ViewModel.Teacher;
using EscuelaDeEsqui.APIs.Application.Conversions;
using EscuelaDeEsqui.Domain.Entities;
using EscuelaDeEsqui.APIs.Application.Services.Teachers;
using EscuelaDeEsqui.APIs.ViewModel;

namespace EscuelaDeEsqui.APIs.Application.Controllers
{
    [ApiController]
    public class TeacherController : ControllerBase
    {
        [HttpPost]
        [Route("api/Teacher/New")]
        public async Task New(Create create)
        {
            TeacherConverter Converter = new TeacherConverter();
            Teacher NewTeacher = Converter.ConvertCreateToTeacher(create);
            CreateService createService = new CreateService(NewTeacher);
            await createService.CallCreate();
        }
        [HttpPost]
        [Route("api/Teacher/SearchForInformation")]
        public async Task<IEnumerable<Search>> SearchForTeacherInformation(Search search)
        {
            SearchForTeacherInformationService searchForInformation = new SearchForTeacherInformationService(search);
            return await searchForInformation.CallSearchForTeachInformation();

        }
        [HttpPost]
        [Route("api/Teacher/TeacherInformation")]
        public async Task<PersonalInformation> TeacherPersonalInformation([FromBody]int TeacherId)
        {
            ViewTeacherInformationService service = new ViewTeacherInformationService();
            TeacherConverter converter = new TeacherConverter();
            return converter.ConvertTeacherToPersonalInformation(await service.CallViewTeacherInformation(TeacherId));
        }
        [HttpPost]
        [Route("api/Teacher/GetEditTeacher")]
        public async Task<Edit> GetEditTeacher([FromBody] int TeacherId)
        {
            GetEditService service = new GetEditService();
            TeacherConverter converter = new TeacherConverter();
            return converter.ConvertTeacherToEdit(await service.CallEdit(TeacherId));            
        }
        [HttpPost]
        [Route("api/Teacher/EditTeacher")]
        public async Task EditTeacher(Edit edit)
        {
            TeacherConverter converter = new TeacherConverter();
            Teacher teacher=converter.ConvertEditToTeacher(edit);
            EditService service = new EditService();
            await service.CallEdit(teacher);
        }
        [HttpPost]
        [Route("api/Teacher/DisactivateTeacher")]
        public async Task DesactivateTeacher([FromBody] int TeacherId)
        {
            DisactivateService service = new DisactivateService();
            Teacher teacher=await service.CallGetEditTeacher(TeacherId);
            teacher = service.DisactivateTeacher(teacher);
            await service.CallDisactivate(teacher);
        }
        [HttpPost]
        [Route("api/Teacher/NewAbsence")]
        public async Task NewAbsence(NewAbsence newAbsence)
        {
            TeacherConverter converter = new TeacherConverter();
            Absence absence=converter.ConvertNewAbsenceToAbsence(newAbsence);
            NewAbsenceService service = new NewAbsenceService();
            await service.CallCreate(absence);
        }
    }
}