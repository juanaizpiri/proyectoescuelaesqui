﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EscuelaDeEsqui.APIs.Application.Conversions;
using EscuelaDeEsqui.APIs.Application.Services.Clients;
using EscuelaDeEsqui.APIs.Application.ViewModel.Client;
using EscuelaDeEsqui.APIs.ViewModel;
using EscuelaDeEsqui.Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EscuelaDeEsqui.APIs.Application.Controllers
{
    [ApiController]
    public class ClientController : ControllerBase
    {
        [HttpPost]
        [Route("api/Client/New")]
        public async Task New(Create create)
        {
            ClientConverter Converter = new ClientConverter();
            Client NewClient = Converter.ConvertCreateToClient(create);
            CreateService createService = new CreateService();
            await createService.CallCreate(NewClient);
        }
        [HttpPost]
        [Route("api/Client/SearchClient")]
        public async Task<IEnumerable<Search>> SearchClient(Search search)
        {
            SearchClientService searchForInformation = new SearchClientService();
            return await searchForInformation.CallSearchClient(search);
        }
        [HttpPost]
        [Route("api/Client/ClientInformation")]
        public async Task<PersonalInformation> TeacherPersonalInformation([FromBody]int ClientId)
        {
            ViewClientInformationService service = new ViewClientInformationService();
            ClientConverter converter = new ClientConverter();
            return converter.ConvertclientToPersonalInformation(await service.CallViewClientInformation(ClientId));
        }
        [HttpPost]
        [Route("api/Client/GetEditClient")]
        public async Task<Edit> GetEditClient([FromBody] int ClientId)
        {
            GetEditService service = new GetEditService();
            ClientConverter converter = new ClientConverter();
            return converter.ConvertclientToEdit(await service.CallEdit(ClientId));
        }
        [HttpPost]
        [Route("api/Client/EditClient")]
        public async Task EditTeacher(Edit edit)
        {
            ClientConverter converter = new ClientConverter();
            Client client = converter.ConvertEditToClient(edit);
            EditService service = new EditService();
            await service.CallEdit(client);
        }
    }
}