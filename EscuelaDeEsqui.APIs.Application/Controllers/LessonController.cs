﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EscuelaDeEsqui.APIs.Application.Conversions;
using EscuelaDeEsqui.APIs.Application.ViewModel.Lesson;
using EscuelaDeEsqui.Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using EscuelaDeEsqui.APIs.Application.Services.Lesson;

namespace EscuelaDeEsqui.APIs.Application.Controllers
{
    [ApiController]
    public class LessonController : ControllerBase
    {
        [HttpPost]
        [Route("api/Lesson/NewLesson")]
        public async Task NewLesson(Create create)
        {
            LessonConverter converter = new LessonConverter();
            Lesson lesson=converter.ConvertCreateToLesson(create);
            GetAllTeachersService allTeacherService = new GetAllTeachersService();
            IEnumerable<Teacher> allTeachers = await allTeacherService.CallGetAllTeacher();
            GetTeacherIdService teacherIdService = new GetTeacherIdService();
            lesson.TeacherId = await teacherIdService.CallGetAllTeacher(allTeachers, lesson.DateAndTime, lesson.Duration);
            CreateLessonService CreateService = new CreateLessonService();
            await CreateService.CallNEWLesson(lesson);
        }

    }
}