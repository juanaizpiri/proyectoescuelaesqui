﻿using EscuelaDeEsqui.APIs.Application.ViewModel.Client;
using EscuelaDeEsqui.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.APIs.Application.Conversions
{
    public class ClientConverter
    {
        public Client ConvertCreateToClient(Create create)
        {
            Client NewClient = new Client()
            {
                Name = create.Name,
                Surname1 = create.Surname1,
                Surname2 = create.Surname2,
                Email = create.Email,
                Phone = create.Phone,
                Nationality = create.Nationality,
                DNI = create.DNI,
                CompanyCIF=create.CompanyCIF,
                CompanyName=create.CompanyName
            };
            return NewClient;
        }
        public PersonalInformation ConvertclientToPersonalInformation(Client client)
        {
            PersonalInformation personalInformation = new PersonalInformation()
            {
                Name = client.Name,
                Surname1 = client.Surname1,
                Surname2 = client.Surname2,
                Email = client.Email,
                Phone = client.Phone,
                Nationality = client.Nationality,
                DNI = client.DNI,
                CompanyCIF = client.CompanyCIF,
                CompanyName = client.CompanyName
            };
            return personalInformation;
        }
        public Edit ConvertclientToEdit(Client client)
        {
            Edit edit = new Edit()
            {
                Name = client.Name,
                Surname1 = client.Surname1,
                Surname2 = client.Surname2,
                Email = client.Email,
                Phone = client.Phone,
                Nationality = client.Nationality,
                DNI = client.DNI,
                ClientId = client.ClientId,
                CompanyCIF=client.CompanyCIF,
                CompanyName=client.CompanyName
            };
            return edit;
        }
        public Client ConvertEditToClient(Edit edit)
        {
            Client EditClient = new Client()
            {
                Name = edit.Name,
                Surname1 = edit.Surname1,
                Surname2 = edit.Surname2,
                Email = edit.Email,
                Phone = edit.Phone,
                Nationality = edit.Nationality,
                DNI = edit.DNI,
                ClientId = edit.ClientId,
                CompanyName=edit.CompanyName,
                CompanyCIF=edit.CompanyCIF
            };
            return EditClient;
        }
    }
}
