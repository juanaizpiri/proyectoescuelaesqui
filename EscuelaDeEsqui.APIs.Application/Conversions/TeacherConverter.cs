﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EscuelaDeEsqui.APIs.Application.ViewModel.Teacher;
using EscuelaDeEsqui.Domain.Entities;
using EscuelaDeEsqui.Domain.ValueObjects.Payment;

namespace EscuelaDeEsqui.APIs.Application.Conversions
{
    public class TeacherConverter
    {
        public Teacher ConvertCreateToTeacher(Create create)
        {
            BankAccountInformation NewBankAccountInformation = new BankAccountInformation(create.IBAN,
                create.BankIdentifier, create.BranchIdentifier, create.AccountNumber);
            Teacher NewTeacher = new Teacher()
            {
                Name = create.Name,
                Surname1 = create.Surname1,
                Surname2 = create.Surname2,
                Email = create.Email,
                Phone = create.Phone,
                Nationality = create.Nationality,
                SocialSecurityNumber = create.SocialSecurityNumber,
                IRPFPercentage = create.IRPFPerfectage,
                bankAccountInformation = NewBankAccountInformation,
                Tittle = create.Tittle,
                Salary = create.Salary,
                DNI=create.DNI
            };
            return NewTeacher;
        }
        public PersonalInformation ConvertTeacherToPersonalInformation(Teacher teacher)
        {
            PersonalInformation personalInformation = new PersonalInformation()
            {
                Name = teacher.Name,
                Surname1 = teacher.Surname1,
                Surname2 = teacher.Surname2,
                Email = teacher.Email,
                Phone = teacher.Phone,
                Nationality = teacher.Nationality,
                SocialSecurityNumber = teacher.SocialSecurityNumber,
                IRPFPerfectage = teacher.IRPFPercentage,
                IBAN = teacher.bankAccountInformation.IBAN,
                BankIdentifier=teacher.bankAccountInformation.BankIdentifier,
                BranchIdentifier=teacher.bankAccountInformation.BranchIdentifier,
                AccountNumber=teacher.bankAccountInformation.AccountNumber,
                State=teacher.State,
                Tittle = teacher.Tittle,
                Salary = teacher.Salary,
                DNI = teacher.DNI
            };
            return personalInformation;
        }
        public Edit ConvertTeacherToEdit(Teacher teacher)
        {
            Edit edit = new Edit()
            {
                Name = teacher.Name,
                Surname1 = teacher.Surname1,
                Surname2 = teacher.Surname2,
                Email = teacher.Email,
                Phone = teacher.Phone,
                Nationality = teacher.Nationality,
                SocialSecurityNumber = teacher.SocialSecurityNumber,
                IRPFPerfectage = teacher.IRPFPercentage,
                IBAN = teacher.bankAccountInformation.IBAN,
                BankIdentifier = teacher.bankAccountInformation.BankIdentifier,
                BranchIdentifier = teacher.bankAccountInformation.BranchIdentifier,
                AccountNumber = teacher.bankAccountInformation.AccountNumber,
                State=teacher.State,
                Tittle = teacher.Tittle,
                Salary = teacher.Salary,
                DNI = teacher.DNI,
                TeacherId=teacher.TeacherId
            };
            return edit;
        }
        public Teacher ConvertEditToTeacher(Edit edit)
        {
            BankAccountInformation NewBankAccountInformation = new BankAccountInformation(edit.IBAN,
                 edit.BankIdentifier, edit.BranchIdentifier, edit.AccountNumber);
            Teacher EditTeacher = new Teacher()
            {
                Name = edit.Name,
                Surname1 = edit.Surname1,
                Surname2 = edit.Surname2,
                Email = edit.Email,
                Phone = edit.Phone,
                Nationality = edit.Nationality,
                SocialSecurityNumber = edit.SocialSecurityNumber,
                IRPFPercentage = edit.IRPFPerfectage,
                bankAccountInformation = NewBankAccountInformation,
                Tittle = edit.Tittle,
                Salary = edit.Salary,
                DNI = edit.DNI,
                TeacherId = edit.TeacherId,
                State=edit.State
            };
            return EditTeacher;
        }
        public Absence ConvertNewAbsenceToAbsence(NewAbsence newAbsence)
        {
            Absence absence = new Absence()
            {
                TeacherId = newAbsence.TeacherId,
                date = newAbsence.date,
                Comments = newAbsence.Comments,
                AbsenceType = newAbsence.AbsenceType,
                Duration = newAbsence.Duration
            };
            return absence;
        }
    }
}
