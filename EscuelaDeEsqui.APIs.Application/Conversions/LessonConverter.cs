﻿using EscuelaDeEsqui.APIs.Application.ViewModel.Lesson;
using EscuelaDeEsqui.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.APIs.Application.Conversions
{
    public class LessonConverter
    {
        public Lesson ConvertCreateToLesson(Create create)
        {
            Lesson lesson = new Lesson
            {
                Duration = create.Duration,
                Comments = create.Comments,
                DateAndTime=create.DateAndTime
            };
            return lesson;
        }
    }
}
