﻿using EscuelaDeEsqui.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.APIs.Application.Services.Teachers
{
    public class EditService
    {
        private HttpClient Client;
        public EditService()
        {
            Client = new HttpClient();
        }
        public async Task CallEdit(Teacher teacher)
        {
            Client.BaseAddress = new Uri("https://localhost:5001/");
            HttpResponseMessage response = await Client.PostAsJsonAsync("api/Teacher/EditTeacher", teacher);
            response.EnsureSuccessStatusCode();
        }
    }
}
