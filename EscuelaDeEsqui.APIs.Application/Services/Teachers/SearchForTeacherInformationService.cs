﻿using EscuelaDeEsqui.APIs.ViewModel;
using EscuelaDeEsqui.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.APIs.Application.Services.Teachers
{
    public class SearchForTeacherInformationService
    {
        private HttpClient Client;
        private Search search;
        public SearchForTeacherInformationService(Search search)
        {
            Client = new HttpClient();
            this.search = search;
        }
        public async Task<IEnumerable<Search>> CallSearchForTeachInformation()
        {
            Client.BaseAddress = new Uri("https://localhost:5001/");
            HttpResponseMessage response = await Client.PostAsJsonAsync("api/Teacher/SearchForInformation", search);
            response.EnsureSuccessStatusCode();
            IEnumerable<Search> searches = await response.Content.ReadAsAsync<IEnumerable<Search>>();
            return searches;
        }
    }
}
