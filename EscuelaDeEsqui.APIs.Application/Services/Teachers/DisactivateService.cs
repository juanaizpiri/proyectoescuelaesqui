﻿using EscuelaDeEsqui.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.APIs.Application.Services.Teachers
{
    public class DisactivateService
    {
        private HttpClient Client;
        public DisactivateService()
        {
        }
        public async Task CallDisactivate(Teacher teacher)
        {
            Client = new HttpClient();
            Client.BaseAddress = new Uri("https://localhost:5001/");
            HttpResponseMessage response = await Client.PostAsJsonAsync("api/Teacher/DisactivateTeacher", teacher);
            response.EnsureSuccessStatusCode();
        }
        public async Task<Teacher> CallGetEditTeacher(int TeacherId)
        {
            Client = new HttpClient();
            Client.BaseAddress = new Uri("https://localhost:5001/");
            HttpResponseMessage response = await Client.PostAsJsonAsync("api/Teacher/GetEditTeacher", TeacherId);
            response.EnsureSuccessStatusCode();
            Teacher teacher = await response.Content.ReadAsAsync<Teacher>();
            return teacher;
        }
        public Teacher DisactivateTeacher(Teacher teacher)
        {
            teacher.State = Teacher.StateEnum.NotActive;
            return teacher;
        }
    }
}
