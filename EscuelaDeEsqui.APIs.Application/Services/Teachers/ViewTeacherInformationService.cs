﻿using EscuelaDeEsqui.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.APIs.Application.Services.Teachers
{
    public class ViewTeacherInformationService
    {
        private HttpClient Client;
        public ViewTeacherInformationService()
        {
            Client = new HttpClient();
        }
        public async Task<Teacher> CallViewTeacherInformation(int TeacherId)
        {
            Client.BaseAddress = new Uri("https://localhost:5001/");
            var response = await Client.PostAsJsonAsync("api/Teacher/TeacherInformation", TeacherId);
            response.EnsureSuccessStatusCode();
            Teacher teacher= await response.Content.ReadAsAsync<Teacher>();
            return teacher;
        }
    }
}
