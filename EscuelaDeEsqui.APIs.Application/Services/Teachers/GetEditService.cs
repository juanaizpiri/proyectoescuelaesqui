﻿using EscuelaDeEsqui.APIs.Application.ViewModel.Teacher;
using EscuelaDeEsqui.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.APIs.Application.Services.Teachers
{
    public class GetEditService
    {
        private HttpClient Client;
        public GetEditService()
        {
            Client = new HttpClient();
        }
        public async Task<Teacher> CallEdit(int TeacherId)
        {
            Client.BaseAddress = new Uri("https://localhost:5001/");
            HttpResponseMessage response = await Client.PostAsJsonAsync("api/Teacher/GetEditTeacher", TeacherId);
            response.EnsureSuccessStatusCode();
            return (await response.Content.ReadAsAsync<Teacher>());
        }
    }
}
