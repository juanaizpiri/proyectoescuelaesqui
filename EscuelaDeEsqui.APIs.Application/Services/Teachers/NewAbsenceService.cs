﻿using EscuelaDeEsqui.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.APIs.Application.Services.Teachers
{
    public class NewAbsenceService
    {
        private HttpClient Client;
        public NewAbsenceService()
        {
            Client = new HttpClient();
        }
        public async Task CallCreate(Absence absence)
        {
            Client.BaseAddress = new Uri("https://localhost:5001/");
            HttpResponseMessage response = await Client.PostAsJsonAsync("api/Teacher/NewAbsence", absence);
            response.EnsureSuccessStatusCode();
        }
    }
}

