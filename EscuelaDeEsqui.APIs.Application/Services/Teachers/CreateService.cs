﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using EscuelaDeEsqui.Domain.Entities;

namespace EscuelaDeEsqui.APIs.Application.Services.Teachers
{
    public class CreateService
    {
        private HttpClient Client;
        private Teacher NewTeacher;
        public CreateService(Teacher teacher)
        {
            Client = new HttpClient();
            NewTeacher = teacher;
        }
        public async Task CallCreate()
        {
            Client.BaseAddress = new Uri("https://localhost:5001/");
            HttpResponseMessage response = await Client.PostAsJsonAsync("api/Teacher/New", NewTeacher);
            response.EnsureSuccessStatusCode();
        }
    }
}
