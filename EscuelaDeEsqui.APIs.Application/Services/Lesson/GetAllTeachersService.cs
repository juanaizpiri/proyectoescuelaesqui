﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.APIs.Application.Services.Lesson
{
    public class GetAllTeachersService
    {
        private HttpClient Client;
        public GetAllTeachersService()
        {

        }
        public async Task<IEnumerable<Domain.Entities.Teacher>> CallGetAllTeacher()
        {
            Client = new HttpClient();
            Client.BaseAddress = new Uri("https://localhost:5001/");
            HttpResponseMessage response = await Client.GetAsync("api/Teacher/GetAllTeachers");
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<IEnumerable<Domain.Entities.Teacher>>();
        }
    }
}
