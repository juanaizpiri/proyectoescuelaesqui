﻿using EscuelaDeEsqui.APIs.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.APIs.Application.Services.Lesson
{
    public class GetTeacherIdService
    {
        private HttpClient Client;
        public GetTeacherIdService()
        {

        }
        public async Task<int> CallGetAllTeacher(IEnumerable<Domain.Entities.Teacher> AllTeachers, DateTime Date, TimeSpan Duration)
        {
            Client = new HttpClient();
            Client.BaseAddress = new Uri("https://localhost:5001/");
            GetTeacherId ViewModel = new GetTeacherId()
            {
                teachers = AllTeachers,
                Date = Date,
                Duration = Duration
            };
            HttpResponseMessage response = await Client.PostAsJsonAsync("api/Lesson/FreeTeacher",ViewModel);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<int>();
        }
    }
}
