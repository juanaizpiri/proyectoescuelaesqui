﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.APIs.Application.Services.Lesson
{
    public class SelectTeacherService
    {
        HttpClient Client;
        public SelectTeacherService()
        {

        }
        public async Task GetTeacher(DateTime DateAndTime)
        {
           
            Client = new HttpClient();
            Client.BaseAddress = new Uri("https://localhost:5001/");
            HttpResponseMessage response = await Client.PostAsJsonAsync("api/Teacher/GetFreeTeacher", DateAndTime);
            response.EnsureSuccessStatusCode();
        }
    }
}
