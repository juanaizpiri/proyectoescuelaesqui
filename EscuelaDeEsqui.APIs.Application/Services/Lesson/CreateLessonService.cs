﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using EscuelaDeEsqui.Domain.Entities;

namespace EscuelaDeEsqui.APIs.Application.Services.Lesson
{
    public class CreateLessonService
    {
        private HttpClient Client;
        public CreateLessonService()
        {

        }
        public async Task CallNEWLesson(Domain.Entities.Lesson lesson)
        {
            Client = new HttpClient();
            Client.BaseAddress = new Uri("https://localhost:5001/");
            HttpResponseMessage response = await Client.PostAsJsonAsync("api/Lesson/New", lesson);
            response.EnsureSuccessStatusCode();
        }
    }
}
