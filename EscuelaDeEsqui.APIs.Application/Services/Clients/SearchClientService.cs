﻿using EscuelaDeEsqui.APIs.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.APIs.Application.Services.Clients
{
    public class SearchClientService
    {
        private HttpClient Client;
        public SearchClientService()
        {
            
        }
        public async Task<IEnumerable<Search>> CallSearchClient(Search search)
        {
            Client = new HttpClient();
            Client.BaseAddress = new Uri("https://localhost:5001/");
            HttpResponseMessage response = await Client.PostAsJsonAsync("api/Client/SearchClient", search);
            response.EnsureSuccessStatusCode();
            IEnumerable<Search> searches = await response.Content.ReadAsAsync<IEnumerable<Search>>();
            return searches;
        }
    }
}
