﻿using EscuelaDeEsqui.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.APIs.Application.Services.Clients
{
    public class ViewClientInformationService
    {
        private HttpClient Client;
        public ViewClientInformationService()
        {
            
        }
        public async Task<Client> CallViewClientInformation(int ClientId)
        {
            Client = new HttpClient();
            Client.BaseAddress = new Uri("https://localhost:5001/");
            var response = await Client.PostAsJsonAsync("api/Client/ClientInformation", ClientId);
            response.EnsureSuccessStatusCode();
            Client client = await response.Content.ReadAsAsync<Client>();
            return client;
        }
    }
}
