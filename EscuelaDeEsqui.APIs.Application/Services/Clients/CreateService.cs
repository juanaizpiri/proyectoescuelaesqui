﻿using EscuelaDeEsqui.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.APIs.Application.Services.Clients
{
    public class CreateService
    {
        private HttpClient Client;
        public CreateService()
        {
            
        }
        public async Task CallCreate(Client client)
        {
            Client = new HttpClient();
            Client.BaseAddress = new Uri("https://localhost:5001/");
            HttpResponseMessage response = await Client.PostAsJsonAsync("api/Client/New", client);
            response.EnsureSuccessStatusCode();
        }
    }
}
