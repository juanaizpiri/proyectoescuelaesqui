﻿using EscuelaDeEsqui.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.APIs.Application.Services.Clients
{
    public class GetEditService
    {
        private HttpClient Client;
        public GetEditService()
        {
            
        }
        public async Task<Client> CallEdit(int ClientId)
        {
            Client = new HttpClient();
            Client.BaseAddress = new Uri("https://localhost:5001/");
            HttpResponseMessage response = await Client.PostAsJsonAsync("api/Client/GetEditClient", ClientId);
            response.EnsureSuccessStatusCode();
            return (await response.Content.ReadAsAsync<Client>());
        }
    }
}
