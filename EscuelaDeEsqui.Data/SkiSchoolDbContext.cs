﻿using System;
using System.Collections.Generic;
using System.Text;
using EscuelaDeEsqui.Data.Configuration;
using EscuelaDeEsqui.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace EscuelaDeEsqui.Data
{
    public class SkiSchoolDbContext : DbContext
    {
        public SkiSchoolDbContext(DbContextOptions<SkiSchoolDbContext> options) : base(options)
        {
        }
        public DbSet<Client> Clientes { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Lesson> Lessons { get; set; }
        public DbSet<LessonStudents> lessonStudents { get; set; }
        public DbSet<LessonType> LessonTypes { get; set; }
        public DbSet<Bill> Bills { get; set; }
        public DbSet<BillLine> BillLines { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Roster> Rosters { get; set; }
        public DbSet<Absence> Absences { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new AbsenceConfiguration());
            modelBuilder.ApplyConfiguration(new BillConfiguration());
            modelBuilder.ApplyConfiguration(new BillLineConfiguration());
            modelBuilder.ApplyConfiguration(new ClientConfiguration());
            modelBuilder.ApplyConfiguration(new ConfigurationLessonType());
            modelBuilder.ApplyConfiguration(new LessonConfiguration());
            modelBuilder.ApplyConfiguration(new LessonStudentConfiguration());
            modelBuilder.ApplyConfiguration(new PaymentConfiguration());
            modelBuilder.ApplyConfiguration(new RosterConfiguration());
            modelBuilder.ApplyConfiguration(new StudentConfiguration());
            modelBuilder.ApplyConfiguration(new TeacherConfiguration());
        }
    }
}
