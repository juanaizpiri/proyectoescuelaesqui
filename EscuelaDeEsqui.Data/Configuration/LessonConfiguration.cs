﻿using EscuelaDeEsqui.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EscuelaDeEsqui.Data.Configuration
{
    class LessonConfiguration : IEntityTypeConfiguration<Lesson>
    {
        public void Configure(EntityTypeBuilder<Lesson> builder)
        {
            //PK
            builder.HasKey(x => x.LessonId);
            //FK
            builder.HasMany(x => x.Students).WithOne().HasForeignKey(x=>x.LessonId);
            builder.HasOne(x => x.lessonType).WithMany().HasForeignKey(x => x.LessonId);
            builder.Property(x => x.PricePerStudent).HasColumnType("SMALLMONEY");

        }
    }
}
