﻿using EscuelaDeEsqui.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EscuelaDeEsqui.Data.Configuration
{
    class ConfigurationLessonType : IEntityTypeConfiguration<LessonType>
    {
        public void Configure(EntityTypeBuilder<LessonType> builder)
        {
            //PK
            builder.HasKey(x => x.LesonTypeId);
            builder.Property(x => x.PricePerHourPerStudent).HasColumnType("SMALLMONEY");
        }
    }
}
