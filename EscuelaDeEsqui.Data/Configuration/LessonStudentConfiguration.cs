﻿using EscuelaDeEsqui.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EscuelaDeEsqui.Data.Configuration
{
    class LessonStudentConfiguration : IEntityTypeConfiguration<LessonStudents>
    {

        public void Configure(EntityTypeBuilder<LessonStudents> builder)
        {
            //PK
            builder.HasKey(nameof(LessonStudents.LessonId), nameof(LessonStudents.StudentId));
            

        }
    }
}
