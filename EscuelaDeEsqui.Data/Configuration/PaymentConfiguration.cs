﻿using EscuelaDeEsqui.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EscuelaDeEsqui.Data.Configuration
{
    class PaymentConfiguration : IEntityTypeConfiguration<Payment>
    {
        public void Configure(EntityTypeBuilder<Payment> builder)
        {
            var CreditCards = builder.OwnsOne(x => x.creditCardInformation);
            CreditCards.Property(x => x.CardNumber);
            CreditCards.Property(x => x.Month);
            CreditCards.Property(x => x.Year);
            var BankAccounts = builder.OwnsOne(x => x.bankAccountInformation);
            BankAccounts.Property(x => x.AccountNumber);
            BankAccounts.Property(x => x.BankIdentifier);
            BankAccounts.Property(x => x.BranchIdentifier);
            BankAccounts.Property(x => x.IBAN);
            //PK
            builder.HasKey(x => x.PaymentId);
        }
    }
}
