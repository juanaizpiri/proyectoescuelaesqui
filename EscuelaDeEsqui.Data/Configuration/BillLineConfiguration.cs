﻿using EscuelaDeEsqui.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EscuelaDeEsqui.Data.Configuration
{
    class BillLineConfiguration : IEntityTypeConfiguration<BillLine>
    {
        public void Configure(EntityTypeBuilder<BillLine> builder)
        {
            //Pk
            builder.HasKey(nameof(BillLine.BillId), nameof(BillLine.LessonId));
            builder.Property(x => x.Amount).HasColumnType("SMALLMONEY");
        }
    }
}
