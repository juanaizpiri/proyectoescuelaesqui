﻿using EscuelaDeEsqui.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EscuelaDeEsqui.Data.Configuration
{
    class BillConfiguration : IEntityTypeConfiguration<Bill>
    {
        public void Configure(EntityTypeBuilder<Bill> builder)
        {
            //PK
            builder.HasKey(x => x.BillId);
            builder.Property(x => x.Tax).HasColumnType("SMALLMONEY");
            builder.Property(x => x.TotalAmount).HasColumnType("SMALLMONEY");
            //FK
            builder.HasOne(x => x.client).WithMany(x => x.Bills).HasForeignKey(x => x.BillId);
            builder.HasMany(x => x.billLine).WithOne().HasForeignKey(x=>x.BillId);
            builder.HasMany(x => x.payments).WithOne().HasForeignKey(x => x.BillId);
        }
    }
}
