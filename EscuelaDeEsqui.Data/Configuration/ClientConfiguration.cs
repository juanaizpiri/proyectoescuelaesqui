﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using EscuelaDeEsqui.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EscuelaDeEsqui.Data.Configuration
{
    class ClientConfiguration : IEntityTypeConfiguration<Client>
    {
        public void Configure(EntityTypeBuilder<Client> builder)
        {
            var CreditCards=builder.OwnsMany(x => x.CreditCardInformation);
            CreditCards.HasKey("Id");
            CreditCards.Property(x => x.CardNumber);
            CreditCards.Property(x => x.Month);
            CreditCards.Property(x => x.Year);
            //PK
            builder.HasKey(x => x.ClientId);
            //FK Clientes
            builder.HasMany(x => x.students).WithOne().HasForeignKey(x => x.ClientId);
        }
    }
}
