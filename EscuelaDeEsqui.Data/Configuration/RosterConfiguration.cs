﻿using EscuelaDeEsqui.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EscuelaDeEsqui.Data.Configuration
{
    class RosterConfiguration : IEntityTypeConfiguration<Roster>
    {
        public void Configure(EntityTypeBuilder<Roster> builder)
        {
            var Deductions = builder.OwnsOne(x => x.deductions);
            Deductions.Property(x => x.IRPF).HasColumnType("SMALLMONEY");
            Deductions.Property(x => x.ProfessionalTraining).HasColumnType("SMALLMONEY");
            Deductions.Property(x => x.RetributionBaseSalary).HasColumnType("SMALLMONEY");
            Deductions.Property(x => x.UnemploymentRetribution).HasColumnType("SMALLMONEY");
            var NoQuotationAcurrals = builder.OwnsOne(x => x.noQuotationAcurrals);
            NoQuotationAcurrals.Property(x => x.SubsistenceAllowance).HasColumnType("SMALLMONEY");
            var QuotationAcurrals = builder.OwnsOne(x => x.quotationAccruals);
            QuotationAcurrals.Property(x => x.BaseSalry).HasColumnType("SMALLMONEY");
            QuotationAcurrals.Property(x => x.Holidays).HasColumnType("SMALLMONEY");
            QuotationAcurrals.Property(x => x.PaymentApportion).HasColumnType("SMALLMONEY");
            QuotationAcurrals.Property(x => x.Transport).HasColumnType("SMALLMONEY");
            var Remuneration = builder.OwnsOne(x => x.remuneration);
            Remuneration.Property(x => x.TotalAccurate).HasColumnType("SMALLMONEY");
            Remuneration.Property(x => x.TotalToDeduct).HasColumnType("SMALLMONEY");
            Remuneration.Property(x => x.TotalToRecieve).HasColumnType("SMALLMONEY");
            var SettlementPeriod = builder.OwnsOne(x => x.settlementPeriod);
            SettlementPeriod.Property(x => x.EndDate);
            SettlementPeriod.Property(x => x.StartDate);
            //PK
            builder.HasKey(x => x.RosterId);
            
        }
    }
}
