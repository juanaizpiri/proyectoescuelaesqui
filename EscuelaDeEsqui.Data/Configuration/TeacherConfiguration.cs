﻿using EscuelaDeEsqui.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EscuelaDeEsqui.Data.Configuration
{
    class TeacherConfiguration : IEntityTypeConfiguration<Teacher>
    {
        public void Configure(EntityTypeBuilder<Teacher> builder)
        {
            var BankAccountInformation = builder.OwnsOne(x => x.bankAccountInformation);
            BankAccountInformation.Property(x => x.AccountNumber);
            BankAccountInformation.Property(x => x.BankIdentifier);
            BankAccountInformation.Property(x => x.BranchIdentifier);
            BankAccountInformation.Property(x => x.IBAN);
            //PK
            builder.HasKey(x => x.TeacherId);
            //FK
            builder.HasMany(x => x.Lessons).WithOne().HasForeignKey(x => x.LessonId);
            builder.HasMany(x => x.Absences).WithOne().HasForeignKey(x => x.TeacherId);
            builder.HasMany(x => x.Rosters).WithOne().HasForeignKey(x => x.TeacherId);
            builder.Property(x => x.Salary).HasColumnType("SMALLMONEY");
        }
    }
}
