﻿using EscuelaDeEsqui.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EscuelaDeEsqui.Data.Configuration
{
    class AbsenceConfiguration: IEntityTypeConfiguration<Absence>
    {
        public void Configure(EntityTypeBuilder<Absence> builder)
        {
            //PK
            builder.HasKey(nameof(Absence.TeacherId), nameof(Absence.date));
        }
    }
}
