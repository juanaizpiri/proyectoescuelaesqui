﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EscuelaDeEsqui.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Clientes",
                columns: table => new
                {
                    ClientId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Surname1 = table.Column<string>(maxLength: 50, nullable: false),
                    Surname2 = table.Column<string>(maxLength: 50, nullable: true),
                    Email = table.Column<string>(nullable: false),
                    Phone = table.Column<string>(nullable: true),
                    Nationality = table.Column<string>(maxLength: 50, nullable: true),
                    DNI = table.Column<string>(maxLength: 9, nullable: true),
                    CompanyName = table.Column<string>(maxLength: 75, nullable: true),
                    CompanyCIF = table.Column<string>(maxLength: 9, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clientes", x => x.ClientId);
                });

            migrationBuilder.CreateTable(
                name: "LessonTypes",
                columns: table => new
                {
                    LesonTypeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 20, nullable: true),
                    MaxNumberOfStudents = table.Column<int>(nullable: false),
                    MinNumberOfStudents = table.Column<int>(nullable: false),
                    PricePerHourPerStudent = table.Column<decimal>(type: "SMALLMONEY", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LessonTypes", x => x.LesonTypeId);
                });

            migrationBuilder.CreateTable(
                name: "Teachers",
                columns: table => new
                {
                    TeacherId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Surname1 = table.Column<string>(maxLength: 50, nullable: false),
                    Surname2 = table.Column<string>(maxLength: 50, nullable: true),
                    DNI = table.Column<string>(maxLength: 9, nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Nationality = table.Column<string>(maxLength: 50, nullable: true),
                    SocialSecurityNumber = table.Column<string>(maxLength: 11, nullable: true),
                    IRPFPercentage = table.Column<decimal>(nullable: false),
                    bankAccountInformation_IBAN = table.Column<string>(maxLength: 4, nullable: true),
                    bankAccountInformation_BankIdentifier = table.Column<string>(maxLength: 4, nullable: true),
                    bankAccountInformation_BranchIdentifier = table.Column<string>(maxLength: 4, nullable: true),
                    bankAccountInformation_AccountNumber = table.Column<string>(maxLength: 10, nullable: true),
                    State = table.Column<int>(nullable: false),
                    Tittle = table.Column<int>(nullable: false),
                    Salary = table.Column<decimal>(type: "SMALLMONEY", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teachers", x => x.TeacherId);
                });

            migrationBuilder.CreateTable(
                name: "Bills",
                columns: table => new
                {
                    BillId = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Tax = table.Column<decimal>(type: "SMALLMONEY", nullable: false),
                    TotalAmount = table.Column<decimal>(type: "SMALLMONEY", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bills", x => x.BillId);
                    table.ForeignKey(
                        name: "FK_Bills_Clientes_BillId",
                        column: x => x.BillId,
                        principalTable: "Clientes",
                        principalColumn: "ClientId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Clientes_CreditCardInformation",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CardNumber = table.Column<string>(nullable: true),
                    Month = table.Column<int>(nullable: false),
                    Year = table.Column<int>(nullable: false),
                    ClientId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clientes_CreditCardInformation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Clientes_CreditCardInformation_Clientes_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clientes",
                        principalColumn: "ClientId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Students",
                columns: table => new
                {
                    StudentId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClientId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Surname1 = table.Column<string>(maxLength: 50, nullable: false),
                    Surname2 = table.Column<string>(maxLength: 50, nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Nationality = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Students", x => x.StudentId);
                    table.ForeignKey(
                        name: "FK_Students_Clientes_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clientes",
                        principalColumn: "ClientId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Absences",
                columns: table => new
                {
                    TeacherId = table.Column<int>(nullable: false),
                    date = table.Column<DateTime>(nullable: false),
                    Duration = table.Column<int>(nullable: false),
                    AbsenceType = table.Column<int>(nullable: false),
                    Comments = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Absences", x => new { x.TeacherId, x.date });
                    table.ForeignKey(
                        name: "FK_Absences_Teachers_TeacherId",
                        column: x => x.TeacherId,
                        principalTable: "Teachers",
                        principalColumn: "TeacherId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Lessons",
                columns: table => new
                {
                    LessonId = table.Column<int>(nullable: false),
                    TeacherId = table.Column<int>(nullable: false),
                    Duration = table.Column<TimeSpan>(nullable: false),
                    DateAndTime = table.Column<DateTime>(nullable: false),
                    Comments = table.Column<string>(nullable: true),
                    PricePerStudent = table.Column<decimal>(type: "SMALLMONEY", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lessons", x => x.LessonId);
                    table.ForeignKey(
                        name: "FK_Lessons_LessonTypes_LessonId",
                        column: x => x.LessonId,
                        principalTable: "LessonTypes",
                        principalColumn: "LesonTypeId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Lessons_Teachers_LessonId",
                        column: x => x.LessonId,
                        principalTable: "Teachers",
                        principalColumn: "TeacherId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Rosters",
                columns: table => new
                {
                    RosterId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TeacherId = table.Column<int>(nullable: false),
                    CompanyName = table.Column<string>(maxLength: 20, nullable: false),
                    CompanyAdress = table.Column<string>(maxLength: 75, nullable: false),
                    EmployeeDNI = table.Column<string>(maxLength: 9, nullable: false),
                    EmployeeSocialSecurityNumber = table.Column<string>(maxLength: 11, nullable: false),
                    CompanyCIF = table.Column<string>(maxLength: 9, nullable: false),
                    ProfessionalCategory = table.Column<string>(maxLength: 20, nullable: false),
                    QuoteGroup = table.Column<int>(nullable: false),
                    settlementPeriod_StartDate = table.Column<DateTime>(nullable: false),
                    settlementPeriod_EndDate = table.Column<DateTime>(nullable: false),
                    NHoras = table.Column<decimal>(nullable: false),
                    quotationAccruals_BaseSalry = table.Column<decimal>(type: "SMALLMONEY", nullable: false),
                    quotationAccruals_Holidays = table.Column<decimal>(type: "SMALLMONEY", nullable: false),
                    quotationAccruals_Transport = table.Column<decimal>(type: "SMALLMONEY", nullable: false),
                    quotationAccruals_PaymentApportion = table.Column<decimal>(type: "SMALLMONEY", nullable: false),
                    noQuotationAcurrals_SubsistenceAllowance = table.Column<decimal>(type: "SMALLMONEY", nullable: false),
                    deductions_RetributionBaseSalary = table.Column<decimal>(type: "SMALLMONEY", nullable: false),
                    deductions_UnemploymentRetribution = table.Column<decimal>(type: "SMALLMONEY", nullable: false),
                    deductions_ProfessionalTraining = table.Column<decimal>(type: "SMALLMONEY", nullable: false),
                    deductions_IRPF = table.Column<decimal>(type: "SMALLMONEY", nullable: false),
                    remuneration_TotalAccurate = table.Column<decimal>(type: "SMALLMONEY", nullable: false),
                    remuneration_TotalToDeduct = table.Column<decimal>(type: "SMALLMONEY", nullable: false),
                    remuneration_TotalToRecieve = table.Column<decimal>(type: "SMALLMONEY", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rosters", x => x.RosterId);
                    table.ForeignKey(
                        name: "FK_Rosters_Teachers_TeacherId",
                        column: x => x.TeacherId,
                        principalTable: "Teachers",
                        principalColumn: "TeacherId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BillLines",
                columns: table => new
                {
                    BillId = table.Column<int>(nullable: false),
                    LessonId = table.Column<int>(nullable: false),
                    Amount = table.Column<decimal>(type: "SMALLMONEY", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillLines", x => new { x.BillId, x.LessonId });
                    table.ForeignKey(
                        name: "FK_BillLines_Bills_BillId",
                        column: x => x.BillId,
                        principalTable: "Bills",
                        principalColumn: "BillId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Payments",
                columns: table => new
                {
                    PaymentId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BillId = table.Column<int>(nullable: false),
                    creditCardInformation_CardNumber = table.Column<string>(nullable: true),
                    creditCardInformation_Month = table.Column<int>(nullable: false),
                    creditCardInformation_Year = table.Column<int>(nullable: false),
                    bankAccountInformation_IBAN = table.Column<string>(maxLength: 4, nullable: true),
                    bankAccountInformation_BankIdentifier = table.Column<string>(maxLength: 4, nullable: true),
                    bankAccountInformation_BranchIdentifier = table.Column<string>(maxLength: 4, nullable: true),
                    bankAccountInformation_AccountNumber = table.Column<string>(maxLength: 10, nullable: true),
                    PaymentAnswer = table.Column<string>(nullable: true),
                    Amount = table.Column<string>(nullable: false),
                    PaymentDate = table.Column<DateTime>(nullable: false),
                    IsPaid = table.Column<bool>(nullable: false),
                    PaymentVia = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payments", x => x.PaymentId);
                    table.ForeignKey(
                        name: "FK_Payments_Bills_BillId",
                        column: x => x.BillId,
                        principalTable: "Bills",
                        principalColumn: "BillId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "lessonStudents",
                columns: table => new
                {
                    LessonId = table.Column<int>(nullable: false),
                    StudentId = table.Column<int>(nullable: false),
                    IsPaid = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_lessonStudents", x => new { x.LessonId, x.StudentId });
                    table.ForeignKey(
                        name: "FK_lessonStudents_Lessons_LessonId",
                        column: x => x.LessonId,
                        principalTable: "Lessons",
                        principalColumn: "LessonId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_lessonStudents_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "StudentId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Clientes_CreditCardInformation_ClientId",
                table: "Clientes_CreditCardInformation",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_lessonStudents_StudentId",
                table: "lessonStudents",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Payments_BillId",
                table: "Payments",
                column: "BillId");

            migrationBuilder.CreateIndex(
                name: "IX_Rosters_TeacherId",
                table: "Rosters",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_Students_ClientId",
                table: "Students",
                column: "ClientId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Absences");

            migrationBuilder.DropTable(
                name: "BillLines");

            migrationBuilder.DropTable(
                name: "Clientes_CreditCardInformation");

            migrationBuilder.DropTable(
                name: "lessonStudents");

            migrationBuilder.DropTable(
                name: "Payments");

            migrationBuilder.DropTable(
                name: "Rosters");

            migrationBuilder.DropTable(
                name: "Lessons");

            migrationBuilder.DropTable(
                name: "Students");

            migrationBuilder.DropTable(
                name: "Bills");

            migrationBuilder.DropTable(
                name: "LessonTypes");

            migrationBuilder.DropTable(
                name: "Teachers");

            migrationBuilder.DropTable(
                name: "Clientes");
        }
    }
}
