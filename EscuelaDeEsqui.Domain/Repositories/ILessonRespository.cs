﻿using EscuelaDeEsqui.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.Domain.Repositories
{
    public interface ILessonRespository
    {
        IQueryable<Lesson> GetAll();
        Task CreateLessonAsync(Lesson NewLesson, CancellationToken Ct = default);
        Task UpdateLessonAsync(Lesson UpdateLesson, CancellationToken Ct = default);
        Task DeleteLessonAsync(int LessonId, CancellationToken Ct = default);
        Task<Lesson> GetLessonByIdAsync(int LessonId, CancellationToken Ct = default);
    }
}
