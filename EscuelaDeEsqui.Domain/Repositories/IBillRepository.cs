﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using EscuelaDeEsqui.Domain.Entities;
using System.Threading.Tasks;
using System.Threading;

namespace EscuelaDeEsqui.Domain.Repositories
{
    public interface IBillRepository
    {
        IQueryable<Bill> GetAll();
        Task CreateBillAsync(Bill bill, CancellationToken Ct=default);
        Task UpdateBillAsync(Bill bill, CancellationToken Ct=default);
        Task DeleteBillAsync(int BillId, CancellationToken Ct=default);
        Task<Bill> GetBillByIdAsync(int BillId, CancellationToken Ct=default);
    }
}
