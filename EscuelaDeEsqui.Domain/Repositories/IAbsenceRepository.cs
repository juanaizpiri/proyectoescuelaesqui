﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using EscuelaDeEsqui.Domain.Entities;
using System.Threading.Tasks;
using System.Threading;

namespace EscuelaDeEsqui.Domain.Repositories
{
    public interface IAbsenceRepository
    {
        IQueryable<Absence> GetAll();
        Task CreateAbsenceAsync(Absence NewAbsence, CancellationToken Ct=default);
        Task<Absence> GetByIdAsync(int TeacherId, DateTime date, CancellationToken Ct=default);
        Task UpdateAbsenceAsync(Absence UpdateAbsence, CancellationToken Ct=default);
        Task DeleteAbsenceAsync(int TeacherId, DateTime date, CancellationToken Ct=default);
    }
}
