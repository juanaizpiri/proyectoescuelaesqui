﻿using EscuelaDeEsqui.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.Domain.Repositories
{
    public interface ILessonTypeRepository
    {
        IQueryable<LessonType> GetAll();
        Task CreateLessonTypeAsync(LessonType NewLessonType, CancellationToken Ct = default);
        Task UpdateLessonTypeAsync(LessonType UpdateLessonType, CancellationToken Ct = default);
        Task DeleteLessonTypeAsync(int LessonTypeId, CancellationToken Ct = default);
        Task<LessonType> GetLessonTypeByIdAsync(int LessonTypeId, CancellationToken Ct = default);
    }
}
