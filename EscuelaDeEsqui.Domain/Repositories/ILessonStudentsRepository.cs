﻿using EscuelaDeEsqui.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.Domain.Repositories
{
    public interface ILessonStudentsRepository
    {
        IQueryable<LessonStudents> GetAll();
        Task CreateLessonStudentsAsync(LessonStudents LessonStudents, CancellationToken Ct = default);
        Task UpdateLessonStudentsAsync(LessonStudents LessonStudents, CancellationToken Ct = default);
        Task DeleteLessonStudentsAsync(int StudentId, int LessonId, CancellationToken Ct = default);
        Task<LessonStudents> GetLessonStudentsById(int BillId, int LessonId, CancellationToken Ct = default);
    }
}
