﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using EscuelaDeEsqui.Domain.Entities;
using System.Threading.Tasks;
using System.Threading;

namespace EscuelaDeEsqui.Domain.Repositories
{
    public interface IBillLineRepository
    {
        IQueryable<BillLine> GetAll();
        Task CreateBillLineAsync(BillLine billLine, CancellationToken Ct=default);
        Task UpdateBillLineAsync(BillLine billLine, CancellationToken Ct = default);
        Task DeleteBillLineAsync(int BillId, int LessonId, CancellationToken Ct = default);
        Task<BillLine> GetBillLineById(int BillId, int LessonId, CancellationToken Ct = default);
    }
}
