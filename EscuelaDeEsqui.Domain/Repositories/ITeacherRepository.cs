﻿using EscuelaDeEsqui.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.Domain.Repositories
{
    public interface ITeacherRepository
    {
        IQueryable<Teacher> GetAll();
        Task CreateTeacherAsync(Teacher NewTeacher, CancellationToken Ct = default);
        Task UpdateTeacherAsync(Teacher UpdateTeacher, CancellationToken Ct = default);
        Task DeleteTeacherAsync(int TeacherId, CancellationToken Ct = default);
        Task<Teacher> GetTeacherByIdAsync(int TeacherId, CancellationToken Ct = default);
    }
}
