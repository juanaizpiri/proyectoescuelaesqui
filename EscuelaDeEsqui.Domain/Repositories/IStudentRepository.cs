﻿using EscuelaDeEsqui.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.Domain.Repositories
{
    public interface IStudentRepository
    {
        IQueryable<Student> GetAll();
        Task CreateStudentAsync(Student NewStudent, CancellationToken Ct = default);
        Task UpdateStudentAsync(Student UpdateStudent, CancellationToken Ct = default);
        Task DeleteStudentAsync(int StudentId, CancellationToken Ct = default);
        Task<Student> GetStudentByIdAsync(int StudentId, CancellationToken Ct = default);
    }
}
