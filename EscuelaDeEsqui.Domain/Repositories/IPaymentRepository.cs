﻿using EscuelaDeEsqui.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.Domain.Repositories
{
    public interface IPaymentRepository
    {
        IQueryable<Payment> GetAll();
        Task CreatePaymentAsync(Payment NewPayment, CancellationToken Ct = default);
        Task UpdatePaymentAsync(Payment UpdatePayment, CancellationToken Ct = default);
        Task DeletePaymentAsync(int PaymentId, CancellationToken Ct = default);
        Task<Payment> GetPaymentByIdAsync(int PaymentId, CancellationToken Ct = default);
    }
}
