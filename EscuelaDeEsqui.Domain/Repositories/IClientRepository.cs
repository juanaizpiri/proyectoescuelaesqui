﻿using System;
using System.Collections.Generic;
using System.Text;
using EscuelaDeEsqui.Domain.Entities;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.Domain.Repositories
{
    public interface IClientRepository
    {
        IQueryable<Client> GetAll();
        Task CreateClientAsync(Client NewClient, CancellationToken Ct = default);
        Task UpdateClientAsync(Client UpdateClient, CancellationToken Ct = default);
        Task DeleteClientAsync(int ClientId, CancellationToken Ct = default);
        Task<Client> GetClientByIdAsync(int ClientId, CancellationToken Ct = default);
    }
}
