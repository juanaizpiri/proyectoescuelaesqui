﻿using EscuelaDeEsqui.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.Domain.Repositories
{
    public interface IRosterRepository
    {
        IQueryable<Roster> GetAll();
        Task CreateRosterAsync(Roster NewRoster, CancellationToken Ct = default);
        Task UpdateRosterAsync(Roster UpdateRoster, CancellationToken Ct = default);
        Task DeleteRosterAsync(int RosterId, CancellationToken Ct = default);
        Task<Roster> GetRosterByIdAsync(int RosterId, CancellationToken Ct = default);
    }
}
