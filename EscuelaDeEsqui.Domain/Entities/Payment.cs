﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using EscuelaDeEsqui.Domain.ValueObjects.Payment;

namespace EscuelaDeEsqui.Domain.Entities
{
    public class Payment
    {
        [Key]
        public int PaymentId { get; set; }
        [Required]
        public int BillId { get; set; }
        [Required]
        [CreditCard]
        public CreditCardInformation creditCardInformation { get; set; }
        public BankAccountInformation bankAccountInformation { get; set; }
        public string PaymentAnswer { get; set; }
        [Required]
        public string Amount { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime PaymentDate { get; set; }
        public bool IsPaid { get; set; } = false;
        public PaymentViaEnum PaymentVia { get; set; }
        public enum PaymentViaEnum
        {
            CreditCard,
            Online,
            Cash,
            Transfer
        }
    }
}
