﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using EscuelaDeEsqui.Domain.ValueObjects.Roster;

namespace EscuelaDeEsqui.Domain.Entities
{
    public class Roster
    {
        [Key]
        public int RosterId { get; set; }
        [Required]
        public int TeacherId { get; set; }
        [Required]
        [StringLength(20)]
        public string CompanyName { get; set; }
        [Required]
        [StringLength(75)]
        public string CompanyAdress { get; set; }
        [Required]
        [StringLength(9)]
        public string EmployeeDNI { get; set; }
        [Required]
        [StringLength(11)]
        public string EmployeeSocialSecurityNumber { get; set; }
        [Required]
        [StringLength(9)]
        public string CompanyCIF { get; set; }
        [Required]
        [StringLength(20)]
        public string ProfessionalCategory { get; set; }
        [Required]
        public int QuoteGroup { get; set; }
        public SettlementPeriod settlementPeriod { get; set; }
        [Required]
        public decimal NHoras { get; set; }
        public QuotationAccruals quotationAccruals { get; set; }
        public NoQuotationAcurrals noQuotationAcurrals { get; set; }
        public Deductions deductions { get; set; }
        public Remuneration remuneration { get; set; }
    }
}
