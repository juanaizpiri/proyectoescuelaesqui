﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EscuelaDeEsqui.Domain.Entities
{
    public class LessonStudents
    {
        public int LessonId { get; set; }
        public int StudentId { get; set; }
        [Required]
        public bool IsPaid { get; set; } = false;
    }
}
