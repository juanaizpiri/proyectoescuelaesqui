﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EscuelaDeEsqui.Domain.Entities
{
    public class Student
    {
        [Key]
        public int StudentId { get; set; }
        [Required]
        public int ClientId { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(50)]
        public string Surname1 { get; set; }
        [StringLength(50)]
        public string Surname2 { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        [Phone]
        public string Phone { get; set; }
        [StringLength(50)]
        public string Nationality { get; set; }
        public List<LessonStudents> Lessons { get; set; }
    }
}
