﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EscuelaDeEsqui.Domain.Entities
{
    public class Bill
    {
        [Key]
        public int BillId { get; set; }
        [Required]
        public Client client { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        public List<BillLine> billLine { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        public decimal Tax { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        public decimal TotalAmount { get; set; }
        public List<Payment> payments { get; set; }
    }
}
