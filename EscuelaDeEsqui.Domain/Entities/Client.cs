﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using EscuelaDeEsqui.Domain.ValueObjects.Payment;

namespace EscuelaDeEsqui.Domain.Entities
{
    public class Client
    {
        [Key]
        public int ClientId { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(50)]
        public string Surname1 { get; set; }
        [StringLength(50)]
        public string Surname2 { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Phone]
        public string Phone { get; set; }
        [StringLength(50)]
        public string Nationality { get; set; }
        [StringLength(9)]
        public string DNI { get; set; }
        public List<CreditCardInformation> CreditCardInformation { get; set;}
        public List<Student> students { get; set; }
        public List<Bill> Bills { get; set; }
        [StringLength(75)]
        public string CompanyName { get; set; }
        [StringLength(9)]
        public string CompanyCIF { get; set; }
    }
}
