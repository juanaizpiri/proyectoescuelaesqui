﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using EscuelaDeEsqui.Domain.ValueObjects.Payment;

namespace EscuelaDeEsqui.Domain.Entities
{
    public class Teacher
    {
       
        [Key]
        public int TeacherId { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(50)]
        public string Surname1 { get; set; }
        [StringLength(50)]
        public string Surname2 { get; set; }
        [StringLength(9)]
        public string DNI { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        [Phone]
        public string Phone { get; set; }
        [StringLength(50)]
        public string Nationality { get; set; }
        [StringLength(11)]
        public string SocialSecurityNumber { get; set; }
        [Required]
        public decimal IRPFPercentage { get; set; }
        public BankAccountInformation bankAccountInformation { get; set; }
        public StateEnum State { get; set; } = StateEnum.Active;
        public List<Lesson> Lessons { get; set; }
        [Required]
        public TittleEnum Tittle { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        public decimal Salary { get; set; }
        public List<Absence> Absences { get; set; }
        public List<Roster> Rosters { get; set; }
        public enum TittleEnum
        {
            TD1,
            TD2,
            TD3
        }
        public enum StateEnum
        {
            Active,
            SickLeave,
            NotCome,
            NotActive
        }
    }
}
