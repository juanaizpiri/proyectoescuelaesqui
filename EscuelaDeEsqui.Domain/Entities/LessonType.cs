﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EscuelaDeEsqui.Domain.Entities
{
    public class LessonType
    {
        [Key]
        public int LesonTypeId { get; set; }
        [StringLength(20)]
        public string Name { get; set; }
        public int MaxNumberOfStudents { get; set; }
        public int MinNumberOfStudents { get; set; }
        [DataType(DataType.Currency)]
        public decimal PricePerHourPerStudent { get; set; }
    }
}
