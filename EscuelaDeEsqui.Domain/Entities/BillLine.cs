﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EscuelaDeEsqui.Domain.Entities
{
    public class BillLine
    {
        [Required]
        public int BillId { get; set; }
        [Required]
        public int LessonId { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        public decimal Amount { get; set; }
    }
}
