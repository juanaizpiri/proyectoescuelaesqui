﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EscuelaDeEsqui.Domain.Entities
{
    public class Lesson
    {
        [Key]
        public int LessonId { get; set; }
        [Required]
        public int TeacherId { get; set; }
        [Required]
        public List<LessonStudents> Students { get; set; }
        public TimeSpan Duration { get; set; }
        public DateTime DateAndTime { get; set; }
        public string Comments { get; set; }
        public LessonType lessonType { get; set; }
        [DataType(DataType.Currency)]
        public decimal PricePerStudent { get; set; }
    }
}
