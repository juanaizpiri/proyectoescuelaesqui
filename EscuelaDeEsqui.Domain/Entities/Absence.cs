﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EscuelaDeEsqui.Domain.Entities
{
    public class Absence
    {
        public int TeacherId{ get; set; }
        [DataType(DataType.Date)]
        public DateTime date { get; set; } = DateTime.Today;
        [Required]
        public DurationEnum Duration{ get; set; }
        [Required]
        public AbsenceTypeEnum AbsenceType { get; set; }
        [StringLength(100)]
        public string Comments { get; set; }
        
        public enum AbsenceTypeEnum
        {
            FreeDay,
            MedicalIssues,
            FamiliarIssues,
            Sick,
            NotJustified,
            Other
        }

        public enum DurationEnum
        {
            AllDay,
            MidDay
        }
    }
}
