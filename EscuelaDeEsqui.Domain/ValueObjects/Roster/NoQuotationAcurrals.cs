﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EscuelaDeEsqui.Domain.ValueObjects.Roster
{
    public class NoQuotationAcurrals
    {
        public NoQuotationAcurrals(decimal subsistenceAllowance)
        {
            SubsistenceAllowance = subsistenceAllowance;
        }

        [DataType(DataType.Currency)]
        public decimal SubsistenceAllowance { get; }
    }
}
