﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EscuelaDeEsqui.Domain.ValueObjects.Roster
{
    public class QuotationAccruals
    {
        public QuotationAccruals(decimal baseSalry, decimal holidays, decimal transport, decimal paymentApportion)
        {
            BaseSalry = baseSalry;
            Holidays = holidays;
            Transport = transport;
            PaymentApportion = paymentApportion;
        }

        [Required]
        [DataType(DataType.Currency)]
        public decimal BaseSalry { get;  }
        [DataType(DataType.Currency)]
        public decimal Holidays { get;  }
        [DataType(DataType.Currency)]
        public decimal Transport { get;  }
        [DataType(DataType.Currency)]
        public decimal PaymentApportion { get;  }
    }
}
