﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EscuelaDeEsqui.Domain.ValueObjects.Roster
{
    public class SettlementPeriod
    {
        public SettlementPeriod(DateTime startDate, DateTime endDate)
        {
            StartDate = startDate;
            EndDate = endDate;
        }

        [Required]
        [DataType(DataType.Date)]
        public DateTime StartDate { get;  }
        [Required]
        [DataType(DataType.Date)]
        public DateTime EndDate { get;  }
    }
}
