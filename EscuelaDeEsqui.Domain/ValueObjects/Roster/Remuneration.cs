﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EscuelaDeEsqui.Domain.ValueObjects.Roster
{
    public class Remuneration
    {
        public Remuneration(decimal totalAccurate, decimal totalToDeduct, decimal totalToRecieve)
        {
            TotalAccurate = totalAccurate;
            TotalToDeduct = totalToDeduct;
            TotalToRecieve = totalToRecieve;
        }

        [Required]
        [DataType(DataType.Currency)]
        public decimal TotalAccurate { get; }
        [Required]
        [DataType(DataType.Currency)]
        public decimal TotalToDeduct { get; }
        [Required]
        [DataType(DataType.Currency)]
        public decimal TotalToRecieve { get; }
    }
}
