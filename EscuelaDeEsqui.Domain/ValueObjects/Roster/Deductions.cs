﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EscuelaDeEsqui.Domain.ValueObjects.Roster
{
    public class Deductions
    {
        public Deductions(decimal retributionBaseSalary, decimal unemploymentRetribution, decimal professionalTraining, decimal iRPF)
        {
            RetributionBaseSalary = retributionBaseSalary;
            UnemploymentRetribution = unemploymentRetribution;
            ProfessionalTraining = professionalTraining;
            IRPF = iRPF;
        }

        [Required]
        [DataType(DataType.Currency)]
        public decimal RetributionBaseSalary { get; }    
        [Required]
        [DataType(DataType.Currency)]
        public decimal UnemploymentRetribution { get; }
        [Required]
        [DataType(DataType.Currency)]
        public decimal ProfessionalTraining { get; }
        [Required]
        [DataType(DataType.Currency)]
        public decimal IRPF { get; }
    }
}
