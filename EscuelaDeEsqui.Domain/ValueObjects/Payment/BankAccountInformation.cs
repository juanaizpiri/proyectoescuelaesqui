﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EscuelaDeEsqui.Domain.ValueObjects.Payment
{
    public class BankAccountInformation
    {
        [Key]
        private int Id;

        public BankAccountInformation(string iBAN, string bankIdentifier, string branchIdentifier, string accountNumber)
        {
            IBAN = iBAN;
            BankIdentifier = bankIdentifier;
            BranchIdentifier = branchIdentifier;
            AccountNumber = accountNumber;
        }
        [StringLength(4)]
        public string IBAN { get; }
        [StringLength(4)]
        public string BankIdentifier { get; }
        [StringLength(4)]
        public string BranchIdentifier { get; }
        [StringLength(10)]
        public string AccountNumber { get; }
    }
}
