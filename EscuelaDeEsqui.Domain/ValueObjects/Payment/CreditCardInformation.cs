﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EscuelaDeEsqui.Domain.ValueObjects.Payment
{
    public class CreditCardInformation
    {
        [Key]
        private int Id;
        public CreditCardInformation(string cardNumber, int month, int year)
        {
            CardNumber = cardNumber ?? throw new ArgumentNullException(nameof(cardNumber));
            Month = month;
            Year = year;
        }

        [CreditCard]
        public string CardNumber { get; }
        public int Month { get; }
        public int Year { get; }
    }
}
