﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using EscuelaDeEsqui.APIs.Application.ViewModel.Client;
using EscuelaDeEsqui.APIs.ViewModel;
using Microsoft.AspNetCore.Mvc;

namespace EscuelaDeEsqui.Controllers
{
    public class ClientsController : Controller
    {
        public IActionResult Create()
        {
            return View(new Create());
        }

        [HttpPost]
        public async Task<IActionResult> Create(Create create)
        {
            if (ModelState.IsValid)
            {
                HttpClient Client = new HttpClient();
                Client.BaseAddress = new Uri("https://localhost:5002/");
                HttpResponseMessage response = await Client.PostAsJsonAsync("api/Client/New", create);
                response.EnsureSuccessStatusCode();
                return RedirectToAction(nameof(Create));
            }
            else
            {
                return (View());
            }
        }
        public IActionResult SearchClient()
        {
            return View(new SearchResult());
        }
        [HttpPost]
        public async Task<IActionResult> SearchClient(SearchResult searchResult)
        {
            if (ModelState.IsValid)
            {
                HttpClient Client = new HttpClient();
                Client.BaseAddress = new Uri("https://localhost:5002/");
                HttpResponseMessage response = await Client.PostAsJsonAsync("api/Client/SearchClient", searchResult.search);
                response.EnsureSuccessStatusCode();
                searchResult.searches = await response.Content.ReadAsAsync<IEnumerable<Search>>();
                return View(searchResult);
            }
            else
            {
                return View(searchResult);
            }
        }
        [HttpGet]
        public async Task<IActionResult> ViewClientInformation(int ClientId)
        {
            HttpClient Client = new HttpClient();
            Client.BaseAddress = new Uri("https://localhost:5002/");
            HttpResponseMessage response = await Client.PostAsJsonAsync("api/Client/ClientInformation", ClientId);
            response.EnsureSuccessStatusCode();
            return View(await response.Content.ReadAsAsync<PersonalInformation>());
        }
        [HttpPost]
        public IActionResult ViewClientInformation(PersonalInformation personalInformation)
        {
            return RedirectToAction(nameof(SearchClient));
        }
        public async Task<IActionResult> Edit(int ClientId)
        {
            HttpClient Client = new HttpClient();
            Client.BaseAddress = new Uri("https://localhost:5002/");
            HttpResponseMessage response = await Client.PostAsJsonAsync("api/Client/GetEditClient", ClientId);
            response.EnsureSuccessStatusCode();
            return View(await response.Content.ReadAsAsync<Edit>());
        }
        [HttpPost]
        public async Task<IActionResult> Edit(Edit edit)
        {
            if (ModelState.IsValid)
            {
                HttpClient Client = new HttpClient();
                Client.BaseAddress = new Uri("https://localhost:5002/");
                HttpResponseMessage response = await Client.PostAsJsonAsync("api/Client/EditClient", edit);
                response.EnsureSuccessStatusCode();
                return RedirectToAction(nameof(SearchClient));
            }
            else
            {
                return View(edit);
            }
        }
    }
}