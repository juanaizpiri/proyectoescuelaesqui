﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EscuelaDeEsqui.APIs.Application.ViewModel.Teacher;
using System.Net.Http;
using EscuelaDeEsqui.APIs.ViewModel;

namespace EscuelaDeEsqui.Controllers
{
    public class TeachersController : Controller
    {
        public IActionResult Create()
        {
            return View(new Create());
        }

        [HttpPost]
        public async Task<IActionResult> Create(Create create)
        {
            if (ModelState.IsValid)
            {
                HttpClient Client = new HttpClient();
                Client.BaseAddress = new Uri("https://localhost:5002/");
                HttpResponseMessage response = await Client.PostAsJsonAsync("api/Teacher/New", create);
                response.EnsureSuccessStatusCode();
                return RedirectToAction(nameof(Create));
            }
            else
            {
                return (View());
            }
        }
        public IActionResult SearchForTeacherInformation()
        {
            return View(new SearchResult());
        }
        [HttpPost]
        public async Task<IActionResult> SearchForTeacherInformation(SearchResult searchResult)
        {
            if(ModelState.IsValid)
            {
                HttpClient Client = new HttpClient();
                Client.BaseAddress = new Uri("https://localhost:5002/");
                HttpResponseMessage response = await Client.PostAsJsonAsync("api/Teacher/SearchForInformation", searchResult.search);
                response.EnsureSuccessStatusCode();                
                searchResult.searches = await response.Content.ReadAsAsync<IEnumerable<Search>>();
                return View(searchResult);
            }
            else
            {
                return View(searchResult);
            }
        }
        [HttpGet]
        public async Task<IActionResult> ViewTeacherInformation(int TeacherId)
        {
            HttpClient Client = new HttpClient();
            Client.BaseAddress = new Uri("https://localhost:5002/");
            HttpResponseMessage response = await Client.PostAsJsonAsync("api/Teacher/TeacherInformation", TeacherId);
            response.EnsureSuccessStatusCode();
            return View(await response.Content.ReadAsAsync<PersonalInformation>());
        }
        [HttpPost]
        public IActionResult ViewTeacherInformation(PersonalInformation personalInformation)
        {
            return RedirectToAction(nameof(SearchForTeacherInformation));
        }
       public async Task<IActionResult> Edit(int TeacherId)
       {
            HttpClient Client = new HttpClient();
            Client.BaseAddress = new Uri("https://localhost:5002/");
            HttpResponseMessage response = await Client.PostAsJsonAsync("api/Teacher/GetEditTeacher", TeacherId);
            response.EnsureSuccessStatusCode();
            return View(await response.Content.ReadAsAsync<Edit>());
       }
       [HttpPost]
       public async Task<IActionResult> Edit(Edit edit)
       {
            if (ModelState.IsValid)
            {
                HttpClient Client = new HttpClient();
                Client.BaseAddress = new Uri("https://localhost:5002/");
                HttpResponseMessage response = await Client.PostAsJsonAsync("api/Teacher/EditTeacher", edit);
                response.EnsureSuccessStatusCode();
                return RedirectToAction(nameof(SearchForTeacherInformation));
            }
            else
            {
                return View(edit);
            }
       }
       public async Task<IActionResult> Disactive(int TeacherId)
       {
            HttpClient Client = new HttpClient();
            Client.BaseAddress = new Uri("https://localhost:5002/");
            HttpResponseMessage response = await Client.PostAsJsonAsync("api/Teacher/DisactivateTeacher", TeacherId);
            response.EnsureSuccessStatusCode();
            return RedirectToAction(nameof(SearchForTeacherInformation));
       }
       [HttpGet]
       public IActionResult Absence(int TeacherId)
       {
            return View(new NewAbsence());
       }
       [HttpPost]
       public async Task<IActionResult> Absence(NewAbsence newAbsence)
       {
            if(ModelState.IsValid)
            {
                HttpClient Client = new HttpClient();
                Client.BaseAddress = new Uri("https://localhost:5002/");
                HttpResponseMessage response = await Client.PostAsJsonAsync("api/Teacher/NewAbsence", newAbsence);
                response.EnsureSuccessStatusCode();
                return RedirectToAction(nameof(SearchForTeacherInformation));
            }
            else
            {
                return View(newAbsence);
            }
       }
    }
}