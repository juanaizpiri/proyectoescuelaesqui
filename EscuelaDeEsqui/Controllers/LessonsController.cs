﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using EscuelaDeEsqui.APIs.Application.ViewModel.Lesson;
using Microsoft.AspNetCore.Mvc;

namespace EscuelaDeEsqui.Controllers
{
    public class LessonsController : Controller
    {
        public IActionResult Create()
        {
            return View(new Create());
        }
        [HttpPost]
        public async Task<IActionResult> Create(Create create)
        {
            if (ModelState.IsValid)
            {
                HttpClient Client = new HttpClient();
                Client.BaseAddress = new Uri("https://localhost:5002/");
                HttpResponseMessage response = await Client.PostAsJsonAsync("api/Lesson/NewLesson", create);
                response.EnsureSuccessStatusCode();
                return RedirectToAction(nameof(Create));
            }
            else
            {
                return View(create);
            }
        }
        
    }
}