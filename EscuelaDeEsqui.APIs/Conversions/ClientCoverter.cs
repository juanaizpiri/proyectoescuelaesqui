﻿using EscuelaDeEsqui.APIs.ViewModel;
using EscuelaDeEsqui.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.APIs.Conversions
{
    public class ClientCoverter
    {
        public IEnumerable<Search> ConvertClientToSearch(IEnumerable<Client> clients)
        {
            Search search;
            List<Search> searches = new List<Search>();
            foreach (Client client in clients)
            {
                search = new Search();
                search.Name = client.Name;
                search.Surname1 = client.Surname1;
                search.Surname2 = client.Surname2;
                search.DNI = client.DNI;
                search.TeacherId = client.ClientId;
                searches.Add(search);
            }
            return searches;

        }
    }
}
