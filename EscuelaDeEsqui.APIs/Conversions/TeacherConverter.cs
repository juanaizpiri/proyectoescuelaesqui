﻿using EscuelaDeEsqui.APIs.ViewModel;
using EscuelaDeEsqui.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.APIs.Conversions
{
    public class TeacherConverter
    {
        public TeacherConverter()
        {
        }
        public IEnumerable<Search> ConvertTeacherToSearch(IEnumerable<Teacher> teachers)
        {
            Search search;
            List<Search> searches = new List<Search>();
            foreach (Teacher teacher in teachers)
            {
                search = new Search();
                search.Name = teacher.Name;
                search.Surname1 = teacher.Surname1;
                search.Surname2 = teacher.Surname2;
                search.DNI = teacher.DNI;
                search.TeacherId = teacher.TeacherId;
                searches.Add(search);
            }
            return searches;

        }
    }
}
