﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EscuelaDeEsqui.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using EscuelaDeEsqui.Domain.Repositories;
using EscuelaDeEsqui.Repositories;

namespace EscuelaDeEsqui.APIs
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //DBContext

            services.AddDbContext<SkiSchoolDbContext>(Options =>
            {
                Options.UseSqlServer(Configuration.GetConnectionString("Local"));
            }
            );

            // Repositories

            services.AddScoped<ITeacherRepository, TeacherRepository>();
            services.AddScoped<IAbsenceRepository, AbsenceRepository>();
            services.AddScoped<IClientRepository, ClientRepository>();
            services.AddScoped<ILessonRespository, LessonRepository>();
            services.AddScoped<ILessonStudentsRepository, LessonStudentRepository>();
            services.AddScoped<ILessonTypeRepository, LessonTypeRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
