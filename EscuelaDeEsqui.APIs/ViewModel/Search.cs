﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.APIs.ViewModel
{
    public class Search
    {
        public int TeacherId { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        [StringLength(50)]
        public string Surname1 { get; set; }
        [StringLength(50)]
        public string Surname2 { get; set; }
        [StringLength(9)]
        public string DNI { get; set; }
    }
}
