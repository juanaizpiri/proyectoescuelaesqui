﻿using EscuelaDeEsqui.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.APIs.ViewModel
{
    public class GetTeacherId
    {
        public IEnumerable<Teacher> teachers { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan Duration { get; set; }
    }
}
