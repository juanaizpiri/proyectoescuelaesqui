﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.APIs.ViewModel
{
    public class SearchResult
    {
        public Search search { get; set; }
        public IEnumerable<Search> searches { get; set; } = new List<Search>();
    }
}
