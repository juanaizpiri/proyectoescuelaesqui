﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EscuelaDeEsqui.Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using EscuelaDeEsqui.Domain.Repositories;
using EscuelaDeEsqui.APIs.Sercices;
using Microsoft.EntityFrameworkCore;
using EscuelaDeEsqui.APIs.ViewModel;

namespace EscuelaDeEsqui.APIs.Controllers
{
    
    [ApiController]
    public class LessonController : ControllerBase
    {
        private readonly ILessonRespository _lessonRepository;
        public LessonController(ILessonRespository lessonRepository)
        {
            _lessonRepository = lessonRepository;
        }
        [HttpPost]
        [Route("api/Lesson/New")]
        public async Task NewLesson(Lesson lesson)
        {
            await _lessonRepository.CreateLessonAsync(lesson);
        }
        [HttpPost]
        [Route("api/Lesson/FreeTeacher")]
        public async Task<int> FreeTeachers(GetTeacherId ViewModel)
        {
            DateTime StartDate = ViewModel.Date;
            DateTime EndDate = ViewModel.Date.Add(ViewModel.Duration);
            IEnumerable<int> TeacherId =await (from lesson in _lessonRepository.GetAll()
                                           where (lesson.DateAndTime == StartDate || (StartDate > lesson.DateAndTime && EndDate < lesson.DateAndTime))
                                           select lesson.TeacherId).ToListAsync();
            int teacherId = (from teachers in ViewModel.teachers
                                                 where !TeacherId.Contains(teachers.TeacherId)
                                                 select teachers.TeacherId).Min();
            return teacherId;
        }
    }
}