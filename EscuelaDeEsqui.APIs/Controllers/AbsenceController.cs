﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EscuelaDeEsqui.Domain.Entities;
using EscuelaDeEsqui.Domain.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EscuelaDeEsqui.APIs.Controllers
{
    [ApiController]
    public class AbsenceController : ControllerBase
    {
        private readonly IAbsenceRepository _absenceRepository;
        public AbsenceController(IAbsenceRepository absenceRepository)
        {
            _absenceRepository = absenceRepository;
        }
        [HttpPost]
        [Route("api/Absence/Create")]
        public async Task Create(Absence absence)
        {
            await _absenceRepository.CreateAbsenceAsync(absence);
        }
    }
}