﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EscuelaDeEsqui.Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using EscuelaDeEsqui.Domain.Repositories;
using EscuelaDeEsqui.Repositories;
using System.Threading;
using EscuelaDeEsqui.APIs.ViewModel;
using EscuelaDeEsqui.APIs.Conversions;
using Microsoft.EntityFrameworkCore;
using EscuelaDeEsqui.APIs.Sercices.Teacher;

namespace EscuelaDeEsqui.APIs.Controllers
{
    [ApiController]
    public class TeacherController : ControllerBase
    {
        private readonly ITeacherRepository _teacherRepository;

        public TeacherController(ITeacherRepository teacherRepository)
        {
            _teacherRepository = teacherRepository ?? throw new ArgumentNullException(nameof(teacherRepository));
        }

        [HttpPost]
        [Route("api/Teacher/New")]
        public async Task New(Teacher teacher)
        {
           await _teacherRepository.CreateTeacherAsync(teacher);
        }
        [HttpPost]
        [Route("api/Teacher/SearchForInformation")]
        public async Task<IEnumerable<Search>> SearchForTeacherInformation(Search search)
        {
            IEnumerable<Teacher> teachers = await _teacherRepository.GetAll().Where(x => (string.IsNullOrEmpty(search.Name) 
            || x.Name.Contains(search.Name)) && (string.IsNullOrEmpty(search.Surname1) || x.Surname1.Contains(search.Surname1)) 
            && (string.IsNullOrEmpty(search.Surname2) || (x.Surname2.Contains(search.Surname2))) && (string.IsNullOrEmpty(search.DNI) 
            || (x.DNI.Contains(search.DNI)))).ToListAsync();
            TeacherConverter convertTeacher = new TeacherConverter();
            IEnumerable<Search> searches = convertTeacher.ConvertTeacherToSearch(teachers);
            return searches;
        }
        [HttpPost]
        [Route("api/Teacher/TeacherInformation")]
        public async Task<Teacher> VieTeacherInformation([FromBody]int TeacherId)
        {
            Teacher teacher = await _teacherRepository.GetTeacherByIdAsync(TeacherId);
            return teacher;
        }
        [HttpPost]
        [Route("api/Teacher/GetEditTeacher")]
        public async Task<Teacher> GetEditTeacher([FromBody] int TeacherId)
        {
            Teacher teacher = await _teacherRepository.GetTeacherByIdAsync(TeacherId);
            return teacher;
        }
        [HttpPost]
        [Route("api/Teacher/EditTeacher")]
        public async Task EditTeacher(Teacher teacher)
        {
            await _teacherRepository.UpdateTeacherAsync(teacher);
        }
        [HttpPost]
        [Route("api/Teacher/DisactivateTeacher")]
        public async Task DisactivateTeacher(Teacher teacher)
        {
            await _teacherRepository.UpdateTeacherAsync(teacher);
        }
        [HttpPost]
        [Route("api/Teacher/NewAbsence")]
        public async Task NewAbsence(Absence absence)
        {
            NewAbsenceService service = new NewAbsenceService();
            await service.CallCreate(absence);
        }
        [HttpGet]
        [Route("api/Teacher/GetAllTeachers")]
        public async Task<IEnumerable<Teacher>> GetAll(DateTime DateAndTime)
        {
            return await _teacherRepository.GetAll().Where(x=>x.State==Teacher.StateEnum.Active).ToListAsync();
        }
    }
}