﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EscuelaDeEsqui.APIs.Conversions;
using EscuelaDeEsqui.APIs.ViewModel;
using EscuelaDeEsqui.Domain.Entities;
using EscuelaDeEsqui.Domain.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EscuelaDeEsqui.APIs.Controllers
{
    
    [ApiController]
    public class ClientController : ControllerBase
    {
        private readonly IClientRepository _ClientRepository;
        public ClientController(IClientRepository teacherRepository)
        {
            _ClientRepository = teacherRepository ?? throw new ArgumentNullException(nameof(teacherRepository));
        }

        [HttpPost]
        [Route("api/Client/New")]
        public async Task New(Client client)
        {
            await _ClientRepository.CreateClientAsync(client);
        }
        [HttpPost]
        [Route("api/Client/SearchClient")]
        public async Task<IEnumerable<Search>> SearchForTeacherInformation(Search search)
        {
            IEnumerable<Client> clients = await _ClientRepository.GetAll().Where(x => (string.IsNullOrEmpty(search.Name)
             || x.Name.Contains(search.Name)) && (string.IsNullOrEmpty(search.Surname1) || x.Surname1.Contains(search.Surname1))
             && (string.IsNullOrEmpty(search.Surname2) || (x.Surname2.Contains(search.Surname2))) && (string.IsNullOrEmpty(search.DNI)
             || (x.DNI.Contains(search.DNI)))).ToListAsync();
            ClientCoverter convertTeacher = new ClientCoverter();
            IEnumerable<Search> searches = convertTeacher.ConvertClientToSearch(clients);
            return searches;
        }
        [HttpPost]
        [Route("api/Client/ClientInformation")]
        public async Task<Client> VieTeacherInformation([FromBody]int ClientId)
        {
            Client client = await _ClientRepository.GetClientByIdAsync(ClientId);
            return client;
        }
        [HttpPost]
        [Route("api/Client/GetEditClient")]
        public async Task<Client> GetEditClient([FromBody] int ClientId)
        {
            Client client = await _ClientRepository.GetClientByIdAsync(ClientId);
            return client;
        }
        [HttpPost]
        [Route("api/Client/EditClient")]
        public async Task EditTeacher(Client client)
        {
            await _ClientRepository.UpdateClientAsync(client);
        }
    }
}