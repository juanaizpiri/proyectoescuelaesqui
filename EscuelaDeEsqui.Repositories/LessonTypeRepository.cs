﻿using EscuelaDeEsqui.Data;
using EscuelaDeEsqui.Domain.Entities;
using EscuelaDeEsqui.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.Repositories
{
    public class LessonTypeRepository:ILessonTypeRepository
    {
        private readonly SkiSchoolDbContext Context;
        public LessonTypeRepository(SkiSchoolDbContext context)
        {
            Context = context;
        }
        public async Task CreateLessonTypeAsync(LessonType NewLessonType, CancellationToken Ct = default)
        {
            if (NewLessonType is null)
            {
                throw new ArgumentNullException(nameof(NewLessonType));
            }
            Ct.ThrowIfCancellationRequested();
            await Context.LessonTypes.AddAsync(NewLessonType);
            await Context.SaveChangesAsync();
        }

        public async Task DeleteLessonTypeAsync(int LessonTypeId, CancellationToken Ct = default)
        {
            LessonType LessonType = await GetLessonTypeByIdAsync(LessonTypeId, Ct);
            Ct.ThrowIfCancellationRequested();
            Context.LessonTypes.Remove(LessonType);
            await Context.SaveChangesAsync();
        }

        public IQueryable<LessonType> GetAll()
        {
            return Context.LessonTypes;
        }

        public async Task<LessonType> GetLessonTypeByIdAsync(int LessonTypeId, CancellationToken Ct = default)
        {
            return await Context.LessonTypes.FindAsync(keyValues: new object[] { LessonTypeId }, Ct);
        }

        public async Task UpdateLessonTypeAsync(LessonType UpdateLessonType, CancellationToken Ct = default)
        {
            if (UpdateLessonType is null)
            {
                throw new ArgumentNullException();
            }
            Ct.ThrowIfCancellationRequested();
            Context.LessonTypes.Update(UpdateLessonType);
            await Context.SaveChangesAsync();
        }
    }
}
