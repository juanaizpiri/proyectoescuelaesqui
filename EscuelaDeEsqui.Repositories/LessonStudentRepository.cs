﻿using EscuelaDeEsqui.Data;
using EscuelaDeEsqui.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EscuelaDeEsqui.Domain.Repositories;

namespace EscuelaDeEsqui.Repositories
{
    public class LessonStudentRepository:ILessonStudentsRepository
    {
        private readonly SkiSchoolDbContext Context;
        public LessonStudentRepository(SkiSchoolDbContext context)
        {
            Context = context;
        }
        public async Task CreateLessonStudentsAsync(LessonStudents LessonStudents, CancellationToken Ct = default)
        {
            if (LessonStudents is null)
            {
                throw new ArgumentNullException(nameof(LessonStudents));
            }
            Ct.ThrowIfCancellationRequested();
            await Context.lessonStudents.AddAsync(LessonStudents);
            await Context.SaveChangesAsync();
        }

        public async Task DeleteLessonStudentsAsync(int StudentId, int LessonId, CancellationToken Ct = default)
        {
            LessonStudents LessonStudents = await GetLessonStudentsById(StudentId, LessonId, Ct);
            Ct.ThrowIfCancellationRequested();
            Context.lessonStudents.Remove(LessonStudents);
            await Context.SaveChangesAsync();
        }

        public IQueryable<LessonStudents> GetAll()
        {
            return Context.lessonStudents;
        }

        public async Task<LessonStudents> GetLessonStudentsById(int BillId, int LessonId, CancellationToken Ct = default)
        {
            return await Context.lessonStudents.FindAsync(keyValues: new object[] { BillId, LessonId }, Ct);
        }

        public async Task UpdateLessonStudentsAsync(LessonStudents LessonStudents, CancellationToken Ct = default)
        {
            if (LessonStudents is null)
            {
                throw new ArgumentNullException(nameof(LessonStudents));
            }
            Ct.ThrowIfCancellationRequested();
            Context.lessonStudents.Update(LessonStudents);
            await Context.SaveChangesAsync();
        }
    }
}
