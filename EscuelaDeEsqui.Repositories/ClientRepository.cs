﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EscuelaDeEsqui.Domain.Entities;
using EscuelaDeEsqui.Domain.Repositories;
using EscuelaDeEsqui.Data;

namespace EscuelaDeEsqui.Repositories
{
    public class ClientRepository : IClientRepository
    {
        private readonly SkiSchoolDbContext Context;
        public ClientRepository(SkiSchoolDbContext context)
        {
            Context = context;
        }
        public async Task CreateClientAsync(Client NewClient, CancellationToken Ct = default)
        {
            if(NewClient is null)
            {
                throw new ArgumentNullException(nameof(NewClient));
            }
            Ct.ThrowIfCancellationRequested();
            await Context.Clientes.AddAsync(NewClient);
            await Context.SaveChangesAsync();
        }

        public async Task DeleteClientAsync(int ClientId, CancellationToken Ct = default)
        {
            Client client = await GetClientByIdAsync(ClientId, Ct);
            Ct.ThrowIfCancellationRequested();
            Context.Clientes.Remove(client);
            await Context.SaveChangesAsync();
        }

        public IQueryable<Client> GetAll()
        {
            return Context.Clientes;
        }

        public async Task<Client> GetClientByIdAsync(int ClientId, CancellationToken Ct = default)
        {
            return await Context.Clientes.FindAsync(keyValues: new object[] { ClientId }, Ct);
        }

        public async Task UpdateClientAsync(Client UpdateClient, CancellationToken Ct = default)
        {
            if(UpdateClient is null)
            {
                throw new ArgumentNullException();
            }
            Ct.ThrowIfCancellationRequested();
            Context.Clientes.Update(UpdateClient);
            await Context.SaveChangesAsync();
        }
    }
}
