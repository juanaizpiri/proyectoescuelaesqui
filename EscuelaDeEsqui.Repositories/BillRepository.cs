﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EscuelaDeEsqui.Domain.Entities;
using EscuelaDeEsqui.Domain.Repositories;
using EscuelaDeEsqui.Data;

namespace EscuelaDeEsqui.Repositories
{
    class BillRepository : IBillRepository
    {
        private readonly SkiSchoolDbContext Context;
        public BillRepository(SkiSchoolDbContext context)
        {
            Context = context;
        }
        public async Task CreateBillAsync(Bill bill, CancellationToken Ct = default)
        {
            if(bill is null)
            {
                throw new ArgumentNullException(nameof(bill));
            }
            Ct.ThrowIfCancellationRequested();
            await Context.Bills.AddAsync(bill);
            await Context.SaveChangesAsync();

        }

        public async Task DeleteBillAsync(int BillId, CancellationToken Ct = default)
        {
            Bill bill = await GetBillByIdAsync(BillId, Ct);
            Ct.ThrowIfCancellationRequested();
            Context.Bills.Remove(bill);
            await Context.SaveChangesAsync();
        }

        public IQueryable<Bill> GetAll()
        {
            return Context.Bills;
        }

        public Task<Bill> GetBillByIdAsync(int BillId, CancellationToken Ct = default)
        {
            return Context.Bills.FindAsync(keyValues: new object[] { BillId }, Ct);
        }

        public async Task UpdateBillAsync(Bill bill, CancellationToken Ct = default)
        {
            if(bill is null)
            {
                throw new ArgumentNullException(nameof(bill));
            }
            Ct.ThrowIfCancellationRequested();
            Context.Bills.Update(bill);
            await Context.SaveChangesAsync();
        }
    }
}
