﻿using EscuelaDeEsqui.Data;
using EscuelaDeEsqui.Domain.Entities;
using EscuelaDeEsqui.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.Repositories
{
    public class TeacherRepository:ITeacherRepository
    {
        private readonly SkiSchoolDbContext Context;
        public TeacherRepository(SkiSchoolDbContext context)
        {
            Context = context;
        }
        public async Task CreateTeacherAsync(Teacher NewTeacher, CancellationToken Ct = default)
        {
            if (NewTeacher is null)
            {
                throw new ArgumentNullException(nameof(NewTeacher));
            }
            await Context.Teachers.AddAsync(NewTeacher);
            await Context.SaveChangesAsync();
        }

        public async Task DeleteTeacherAsync(int TeacherId, CancellationToken Ct = default)
        {
            Teacher teacher = await GetTeacherByIdAsync(TeacherId, Ct);
            Ct.ThrowIfCancellationRequested();
            Context.Teachers.Remove(teacher);
            await Context.SaveChangesAsync();
        }

        public IQueryable<Teacher> GetAll()
        {
            return Context.Teachers;
        }

        public async Task<Teacher> GetTeacherByIdAsync(int TeacherId, CancellationToken Ct = default)
        {
            return await Context.Teachers.FindAsync(keyValues: new object[] { TeacherId }, Ct);
        }

        public async Task UpdateTeacherAsync(Teacher UpdateTeacher, CancellationToken Ct = default)
        {
            if (UpdateTeacher is null)
            {
                throw new ArgumentNullException();
            }
            Ct.ThrowIfCancellationRequested();
            Context.Teachers.Update(UpdateTeacher);
            await Context.SaveChangesAsync();
        }
    }
}
