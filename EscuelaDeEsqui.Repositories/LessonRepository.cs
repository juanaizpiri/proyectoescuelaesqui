﻿using EscuelaDeEsqui.Data;
using EscuelaDeEsqui.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EscuelaDeEsqui.Domain.Repositories;

namespace EscuelaDeEsqui.Repositories
{
    public class LessonRepository:ILessonRespository
    {
        private readonly SkiSchoolDbContext Context;
        public LessonRepository(SkiSchoolDbContext context)
        {
            Context = context;
        }
        public async Task CreateLessonAsync(Lesson NewLesson, CancellationToken Ct = default)
        {
            if (NewLesson is null)
            {
                throw new ArgumentNullException(nameof(NewLesson));
            }
            Ct.ThrowIfCancellationRequested();
            await Context.Lessons.AddAsync(NewLesson);
            await Context.SaveChangesAsync();
        }

        public async Task DeleteLessonAsync(int LessonId, CancellationToken Ct = default)
        {
            Lesson Lesson = await GetLessonByIdAsync(LessonId, Ct);
            Ct.ThrowIfCancellationRequested();
            Context.Lessons.Remove(Lesson);
            await Context.SaveChangesAsync();
        }

        public IQueryable<Lesson> GetAll()
        {
            return Context.Lessons;
        }

        public async Task<Lesson> GetLessonByIdAsync(int LessonId, CancellationToken Ct = default)
        {
            return await Context.Lessons.FindAsync(keyValues: new object[] { LessonId }, Ct);
        }

        public async Task UpdateLessonAsync(Lesson UpdateLesson, CancellationToken Ct = default)
        {
            if (UpdateLesson is null)
            {
                throw new ArgumentNullException();
            }
            Ct.ThrowIfCancellationRequested();
            Context.Lessons.Update(UpdateLesson);
            await Context.SaveChangesAsync();
        }
    }
}
