﻿using EscuelaDeEsqui.Data;
using EscuelaDeEsqui.Domain.Entities;
using EscuelaDeEsqui.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.Repositories
{
    class StudentRepository:IStudentRepository
    {
        private readonly SkiSchoolDbContext Context;
        public StudentRepository(SkiSchoolDbContext context)
        {
            Context = context;
        }
        public async Task CreateStudentAsync(Student NewStudent, CancellationToken Ct = default)
        {
            if (NewStudent is null)
            {
                throw new ArgumentNullException(nameof(NewStudent));
            }
            Ct.ThrowIfCancellationRequested();
            await Context.Students.AddAsync(NewStudent);
            await Context.SaveChangesAsync();
        }

        public async Task DeleteStudentAsync(int StudentId, CancellationToken Ct = default)
        {
            Student Student = await GetStudentByIdAsync(StudentId, Ct);
            Ct.ThrowIfCancellationRequested();
            Context.Students.Remove(Student);
            await Context.SaveChangesAsync();
        }

        public IQueryable<Student> GetAll()
        {
            return Context.Students;
        }

        public async Task<Student> GetStudentByIdAsync(int StudentId, CancellationToken Ct = default)
        {
            return await Context.Students.FindAsync(keyValues: new object[] { StudentId }, Ct);
        }

        public async Task UpdateStudentAsync(Student UpdateStudent, CancellationToken Ct = default)
        {
            if (UpdateStudent is null)
            {
                throw new ArgumentNullException();
            }
            Ct.ThrowIfCancellationRequested();
            Context.Students.Update(UpdateStudent);
            await Context.SaveChangesAsync();
        }
    }
}
