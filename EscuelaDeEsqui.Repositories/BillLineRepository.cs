﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EscuelaDeEsqui.Domain.Entities;
using EscuelaDeEsqui.Domain.Repositories;
using EscuelaDeEsqui.Data;

namespace EscuelaDeEsqui.Repositories
{
    class BillLineRepository : IBillLineRepository
    {
        private readonly SkiSchoolDbContext Context;
        public BillLineRepository(SkiSchoolDbContext context)
        {
            Context = context;
        }
        public async Task CreateBillLineAsync(BillLine billLine, CancellationToken Ct = default)
        {
            if(billLine is null)
            {
                throw new ArgumentNullException(nameof(billLine));
            }
            Ct.ThrowIfCancellationRequested();
            await Context.BillLines.AddAsync(billLine);
            await Context.SaveChangesAsync();
        }

        public async Task DeleteBillLineAsync(int BillId, int LessonId, CancellationToken Ct = default)
        {
            BillLine billLine = await GetBillLineById(BillId, LessonId, Ct);
            Ct.ThrowIfCancellationRequested();
            Context.BillLines.Remove(billLine);
            await Context.SaveChangesAsync();
        }

        public IQueryable<BillLine> GetAll()
        {
            return Context.BillLines;
        }

        public async Task<BillLine> GetBillLineById(int BillId, int LessonId, CancellationToken Ct = default)
        {
            return await Context.BillLines.FindAsync(keyValues: new object[] { BillId, LessonId }, Ct);
        }

        public async Task UpdateBillLineAsync(BillLine billLine, CancellationToken Ct = default)
        {
            if(billLine is null)
            {
                throw new ArgumentNullException(nameof(billLine));
            }
            Ct.ThrowIfCancellationRequested();
            Context.BillLines.Update(billLine);
            await Context.SaveChangesAsync();
        }
    }
}
