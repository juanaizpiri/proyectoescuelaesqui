﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EscuelaDeEsqui.Domain.Entities;
using EscuelaDeEsqui.Domain.Repositories;
using EscuelaDeEsqui.Data;

namespace EscuelaDeEsqui.Repositories
{
    public class AbsenceRepository : IAbsenceRepository
    {
        private readonly SkiSchoolDbContext Context;
        public AbsenceRepository(SkiSchoolDbContext context)
        {
            Context = context;
        }
        public async Task CreateAbsenceAsync(Absence NewAbsence, CancellationToken Ct=default)
        {
            if(NewAbsence is null)
            {
                throw new ArgumentNullException(nameof(NewAbsence));
            }
            Ct.ThrowIfCancellationRequested();
            await Context.Absences.AddAsync(NewAbsence);
            Context.SaveChanges();
        }

        public async Task DeleteAbsenceAsync(int TeacherId, DateTime date, CancellationToken Ct=default)
        {
            Absence absence = await GetByIdAsync(TeacherId, date);
            Ct.ThrowIfCancellationRequested();
            Context.Absences.Remove(absence);
            await Context.SaveChangesAsync();
        }
        public IQueryable<Absence> GetAll()
        {
            return Context.Absences;
        }

        public async Task<Absence> GetByIdAsync(int TeacherId, DateTime date, CancellationToken Ct=default)
        {
            return await Context.Absences.FindAsync(keyValues: new object[] { TeacherId, date}, cancellationToken: Ct);
        }

        public async Task UpdateAbsenceAsync(Absence UpdateAbsence, CancellationToken Ct)
        {
            if(UpdateAbsence is null)
            {
                throw new ArgumentNullException(nameof(UpdateAbsence));
            }
            Ct.ThrowIfCancellationRequested();
            Context.Absences.Update(UpdateAbsence);
            await Context.SaveChangesAsync();
        }
    }
}
