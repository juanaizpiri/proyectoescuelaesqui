﻿using EscuelaDeEsqui.Data;
using EscuelaDeEsqui.Domain.Entities;
using EscuelaDeEsqui.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.Repositories
{
    class RosterRepository:IRosterRepository
    {
        private readonly SkiSchoolDbContext Context;
        public RosterRepository(SkiSchoolDbContext context)
        {
            Context = context;
        }
        public async Task CreateRosterAsync(Roster NewRoster, CancellationToken Ct = default)
        {
            if (NewRoster is null)
            {
                throw new ArgumentNullException(nameof(NewRoster));
            }
            Ct.ThrowIfCancellationRequested();
            await Context.Rosters.AddAsync(NewRoster);
            await Context.SaveChangesAsync();
        }

        public async Task DeleteRosterAsync(int RosterId, CancellationToken Ct = default)
        {
            Roster Roster = await GetRosterByIdAsync(RosterId, Ct);
            Ct.ThrowIfCancellationRequested();
            Context.Rosters.Remove(Roster);
            await Context.SaveChangesAsync();
        }

        public IQueryable<Roster> GetAll()
        {
            return Context.Rosters;
        }

        public async Task<Roster> GetRosterByIdAsync(int RosterId, CancellationToken Ct = default)
        {
            return await Context.Rosters.FindAsync(keyValues: new object[] { RosterId }, Ct);
        }

        public async Task UpdateRosterAsync(Roster UpdateRoster, CancellationToken Ct = default)
        {
            if (UpdateRoster is null)
            {
                throw new ArgumentNullException();
            }
            Ct.ThrowIfCancellationRequested();
            Context.Rosters.Update(UpdateRoster);
            await Context.SaveChangesAsync();
        }
    }
}
