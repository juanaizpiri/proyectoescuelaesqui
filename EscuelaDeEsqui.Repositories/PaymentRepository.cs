﻿using EscuelaDeEsqui.Data;
using EscuelaDeEsqui.Domain.Entities;
using EscuelaDeEsqui.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaDeEsqui.Repositories
{
    class PaymentRepository:IPaymentRepository
    {
        private readonly SkiSchoolDbContext Context;
        public PaymentRepository(SkiSchoolDbContext context)
        {
            Context = context;
        }
        public async Task CreatePaymentAsync(Payment NewPayment, CancellationToken Ct = default)
        {
            if (NewPayment is null)
            {
                throw new ArgumentNullException(nameof(NewPayment));
            }
            Ct.ThrowIfCancellationRequested();
            await Context.Payments.AddAsync(NewPayment);
            await Context.SaveChangesAsync();
        }

        public async Task DeletePaymentAsync(int PaymentId, CancellationToken Ct = default)
        {
            Payment Payment = await GetPaymentByIdAsync(PaymentId, Ct);
            Ct.ThrowIfCancellationRequested();
            Context.Payments.Remove(Payment);
            await Context.SaveChangesAsync();
        }

        public IQueryable<Payment> GetAll()
        {
            return Context.Payments;
        }

        public async Task<Payment> GetPaymentByIdAsync(int PaymentId, CancellationToken Ct = default)
        {
            return await Context.Payments.FindAsync(keyValues: new object[] { PaymentId }, Ct);
        }

        public async Task UpdatePaymentAsync(Payment UpdatePayment, CancellationToken Ct = default)
        {
            if (UpdatePayment is null)
            {
                throw new ArgumentNullException();
            }
            Ct.ThrowIfCancellationRequested();
            Context.Payments.Update(UpdatePayment);
            await Context.SaveChangesAsync();
        }
    }
}
